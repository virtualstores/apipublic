//
//  ControlPosResponseView.swift
//  Api
//
//  Created by Théodore Roos on 2020-08-10.
//  Copyright © 2020 Virtual Stores. All rights reserved.
//

import Foundation

public struct ControlPosResponseView: Codable {
    
    public var controlFlag: Int
    
    public var posResponse: PosResponseView
    
    public init(controlFlag: Int, posResponse: PosResponseView) {
        self.controlFlag = controlFlag
        self.posResponse = posResponse
    }
}

public class ItemResultView: Codable {
    public var id: String
    public var valid: Bool?
    
    public init(id: String, valid: Bool?) {
        self.id = id
        self.valid = valid
    }
}
