//
// VisitWeightedPositionDto.swift
//
// Generated by swagger-codegen
// https://github.com/swagger-api/swagger-codegen
//

import Foundation


public struct VisitWeightedPositionDto: Codable {


    public var x: Int64?

    public var y: Int64?

    public var weight: Int64?
    public init(x: Int64?, y: Int64?, weight: Int64?) { 
        self.x = x
        self.y = y
        self.weight = weight
    }

}
