//
// CartItemDto.swift
//
// Generated by swagger-codegen
// https://github.com/swagger-api/swagger-codegen
//

import Foundation


public struct CartItemDto: Codable {


    public var _id: Int64?

    public var ean: String?

    public var name: String?

    public var brand: String?

    public var price: Double?

    public var totalPrice: Double?

    public var articleNumber: String?

    public var itemId: String?

    public var item: ItemDto?

    public var quantity: Int?

    public var unitOfMeasure: String?

    public var controlDiff: Int?
    public init(_id: Int64?, ean: String?, name: String?, brand: String?, price: Double?, totalPrice: Double?, articleNumber: String?, itemId: String?, item: ItemDto?, quantity: Int?, unitOfMeasure: String?, controlDiff: Int?) { 
        self._id = _id
        self.ean = ean
        self.name = name
        self.brand = brand
        self.price = price
        self.totalPrice = totalPrice
        self.articleNumber = articleNumber
        self.itemId = itemId
        self.item = item
        self.quantity = quantity
        self.unitOfMeasure = unitOfMeasure
        self.controlDiff = controlDiff
    }
    public enum CodingKeys: String, CodingKey { 
        case _id = "id"
        case ean
        case name
        case brand
        case price
        case totalPrice
        case articleNumber
        case itemId
        case item
        case quantity
        case unitOfMeasure
        case controlDiff
    }

}
