//
// RtlsOptionDto.swift
//
// Generated by swagger-codegen
// https://github.com/swagger-api/swagger-codegen
//

import Foundation


public struct RtlsOptionDto: Codable {


    public var _id: Int64?

    public var storeId: Int64?

    public var width: Int?

    public var height: Int?

    public var startOffsetX: Int?

    public var startOffsetY: Int?

    public var waitTime: Int?

    public var panId: String?
    public init(_id: Int64?, storeId: Int64?, width: Int?, height: Int?, startOffsetX: Int?, startOffsetY: Int?, waitTime: Int?, panId: String?) { 
        self._id = _id
        self.storeId = storeId
        self.width = width
        self.height = height
        self.startOffsetX = startOffsetX
        self.startOffsetY = startOffsetY
        self.waitTime = waitTime
        self.panId = panId
    }
    public enum CodingKeys: String, CodingKey { 
        case _id = "id"
        case storeId
        case width
        case height
        case startOffsetX
        case startOffsetY
        case waitTime
        case panId
    }

}
