//
// ItemGroupDto.swift
//
// Generated by swagger-codegen
// https://github.com/swagger-api/swagger-codegen
//

import Foundation


public struct ItemGroupDto: Codable {


    public var _id: String?

    public var name: String?

    public var prettyName: String?

    public var imageUrl: String?

    public var itemPosition: ItemPositionDto?

    public var parentItemGroupId: String?
    public init(_id: String?, name: String?, prettyName: String?, imageUrl: String?, itemPosition: ItemPositionDto?, parentItemGroupId: String?) { 
        self._id = _id
        self.name = name
        self.prettyName = prettyName
        self.imageUrl = imageUrl
        self.itemPosition = itemPosition
        self.parentItemGroupId = parentItemGroupId
    }
    public enum CodingKeys: String, CodingKey { 
        case _id = "id"
        case name
        case prettyName
        case imageUrl
        case itemPosition
        case parentItemGroupId
    }

}
