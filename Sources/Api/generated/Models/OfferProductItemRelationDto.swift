//
// OfferProductItemRelationDto.swift
//
// Generated by swagger-codegen
// https://github.com/swagger-api/swagger-codegen
//

import Foundation


public struct OfferProductItemRelationDto: Codable {


    public var offerProductId: Int64?

    public var itemId: String?
    public init(offerProductId: Int64?, itemId: String?) { 
        self.offerProductId = offerProductId
        self.itemId = itemId
    }

}
