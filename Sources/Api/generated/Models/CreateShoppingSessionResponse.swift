//
// CreateShoppingSessionResponse.swift
//
// Generated by swagger-codegen
// https://github.com/swagger-api/swagger-codegen
//

import Foundation


public struct CreateShoppingSessionResponse: Codable {

    public enum State: Int, Codable { 
        case _0 = 0
        case _1 = 1
        case _2 = 2
        case _3 = 3
        case _4 = 4
    }
    public var session: ShoppingSessionDto?
    public var state: State?
    public init(session: ShoppingSessionDto?, state: State?) { 
        self.session = session
        self.state = state
    }

}
