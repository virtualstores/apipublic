
import Foundation
import Domain


let nowhere = ServerConnection (
    id: 0,
    storeId: 0,
    serverUrl: "https//nowhere.com",
    mqttUrl: "https//nowhere.com",
    apiKey: ""
)

public class ApiImpl: Api {
	
	public var loggingLevel: ApiClientLoggingLevel = .light
	
	public var central: CentralApiCollection {
		let central = CentralApiCollectionImpl(connection: centralConnection, clientId: clientId)
		central.apiClient.loggingLevel = loggingLevel
		return central
	}
	
	public var store: StoreApiCollection?
	
	private let centralConnection: ServerConnection
    private let clientId: Int64
    
    public init(centralConnection: ServerConnection, clientId: Int64) {
        self.centralConnection = centralConnection
        self.clientId = clientId
    }
    
	
	@discardableResult
	public func initStoreApi(storeConnection: ServerConnection) -> StoreApiCollection {
		let store = StoreApiCollectionImpl(connection: storeConnection)
		store.apiClient.loggingLevel = loggingLevel
		self.store = store
		return store
	}
}
