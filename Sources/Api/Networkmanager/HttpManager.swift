//
//  HttpManager.swift
//  Api
//
//  Created by Philip Fryklund on 2019-11-25.
//

import Foundation
import Domain



internal enum HttpMethod: String {
	case get     = "GET"
	case post    = "POST"
	case put     = "PUT"
	case patch   = "PATCH"
	case delete  = "DELETE"
	case head    = "HEAD"
	case options = "OPTIONS"
	case trace   = "TRACE"
	case connect = "CONNECT"
}

internal typealias HttpHeaders = [String: [String]]





//MARK: - Serialization

//enum SerializerError: Error {
//	case wrong
//}
//
//protocol Serializer {
//
//	func serialize(data: Data) throws -> Any
//}
//
//struct JsonSerializer<T: Decodable>: Serializer {
//
//	let type: T.Type
//
//	func serialize(data: Data) throws -> Any {
//		return try JSONDecoder().decode(type, from: data)
//	}
//}





//MARK: - Decoding

internal protocol Decoder {
	
	associatedtype Out
	func decode(data: Data) throws -> Out
}

internal struct JsonDecoder<Out>: Decoder {
    private let _decode: (Data) throws -> Out
    init(type: Out.Type) {
        _decode = { data in
            if data.isEmpty {
                return (Any).self as! Out
            }
            let object = try JSONSerialization.jsonObject(with: data, options: [])
            guard let model = object as? Out else {
                fatalError()
            }
            return model
        }
    }
    
    internal func decode(data: Data) throws -> Out {
        return try _decode(data)
    }
}

internal struct StringDecoder: Decoder {
    typealias Out = String
    func decode(data: Data) throws -> String {
        guard let output = String(data: data, encoding: .utf8) else {
            let context = DecodingError.Context(codingPath: [], debugDescription: "Invalid encoding")
            throw DecodingError.dataCorrupted(context)
        }
        return output
        
    }
}

internal extension JsonDecoder where Out: Decodable {
	
	init(decodable type: Out.Type, decoder: JSONDecoder = JSONDecoder()) {
		_decode = { data in
			return try decoder.decode(type, from: data)
		}
	}
}






//MARK: - Encoding

internal protocol BodyEncoder {
	
	func encode(request: RequestBuilder) throws -> Data?
}


internal struct JsonBodyEncoder<T>: BodyEncoder {
	
	internal let object: T
	init(with object: T) {
		self.object = object
	}
	
	internal func encode(request: RequestBuilder) throws -> Data? {
		request.setHeader("Content-Type", value: "application/json")
		return try JSONSerialization.data(withJSONObject: object, options: [])
	}
}

internal extension JsonBodyEncoder where T: Encodable {
	
	func encode(request: RequestBuilder) throws -> Data? {
		request.setHeader("Content-Type", value: "application/json")
		return try JSONEncoder().encode(object)
	}
}

internal struct URLBodyEncoder: BodyEncoder {
	
	internal let parameters: [String: Any]
	
	private func percentEscaped(_ string: String) -> String {
        return string.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
	}
	
	internal func encode(request: RequestBuilder) throws -> Data? {
		let query = parameters.map({
			let key = percentEscaped($0)
			let value = percentEscaped("\($1)")
			return "\(key)=\(value)"
		}).joined(separator: "&")
		
		switch request.method {
        case .get, .head, .delete, .put:
			request.url += "?" + query
			return nil
		default:
			request.setHeader("Content-Type", value: "application/x-www-form-urlencoded; charset=utf-8")
			return query.data(using: .utf8)
		}
	}
}

internal struct FormBodyEncoder: BodyEncoder {
	
	internal struct FormBody {
		let fileName: String?
		let mime: String?
		let data: Data
	}
	
	internal let parameters: [String: Any]
	
	internal func encode(request: RequestBuilder) throws -> Data? {
		struct Boundary {
			static let crlf = "\r\n"
			
			let boundary: String
			let first: String
			let middle: String
			let last: String
			
			init() {
				boundary = String(format: "virtualstores_boundary_%08x%08x", arc4random(), arc4random())
				first = "--\(boundary)\(Boundary.crlf)"
				middle = "\(Boundary.crlf)--\(boundary)\(Boundary.crlf)"
				last = "\(Boundary.crlf)--\(boundary)--\(Boundary.crlf)"
			}
		}
		let boundary = Boundary()
		
		let bodies: [(name: String, body: FormBody)] = parameters.map { key, value in
			if let body = value as? FormBody {
				return (key, body)
			}
			
			return (key, FormBody(fileName: nil, mime: nil, data: String(describing: value).data(using: .utf8)!))
		}
		
		request.setHeader("Content-Type", value: "multipart/form-data; boundary=\(boundary.boundary)")
		
		var httpBody = Data()
		httpBody += boundary.first.data(using: .utf8)!
		
		for (i, e) in bodies.enumerated() {
			let (name, body) = e
			if i > 0 {
				httpBody += boundary.middle.data(using: .utf8)!
			}
			
			var disposition = #"form-data; name="\#(name)""#
			if let fileName = body.fileName {
				disposition += #"; filename="\#(fileName)""#
			}
			
			var headers = ["Content-Disposition": disposition]
			if let mime = body.mime {
				headers["Content-Type"] = mime
			}
			
			httpBody += headers.map({ "\($0): \($1)\(Boundary.crlf)" }).joined().data(using: .utf8)!
			httpBody += Boundary.crlf.data(using: .utf8)!
			
			httpBody += body.data
		}
		
		httpBody += boundary.last.data(using: .utf8)!
		
		return httpBody
	}
}







//MARK: - Request

internal class Request<T> {
	
	internal var task: URLSessionTask?
	internal var startDate: Date?
	
	internal var onResponse: ((Response<T>) -> ())?
	internal var onError: ((ApiError) -> ())?
	internal var onComplete: (() -> ())?
	
	internal init() {}
	
	
	internal func onResponse(_ onResponse: @escaping (Response<T>) -> ()) {
		self.onResponse = onResponse
	}
	
	internal func onError(_ onError: @escaping (ApiError) -> ()) {
		self.onError = onError
	}
	
	internal func onComplete(_ onComplete: @escaping () -> ()) {
		self.onComplete = onComplete
	}
	
	
	@discardableResult
	internal func resume(builder: ((Request<T>) -> ())? = nil) -> Self {
		builder?(self)
		task?.resume()
		startDate = Date()
		return self
	}
	
	internal func suspend() {
		task?.suspend()
	}
	
	internal func cancel() {
		task?.cancel()
	}
}



//MARK: - Request builder

internal final class RequestBuilder: CustomStringConvertible {
	
	internal var method: HttpMethod = .get
	internal var url: String = ""
	internal private(set) var headers: HttpHeaders = [:]
	internal var bodyEncoder: BodyEncoder?
	
	internal var cachePolicy: URLRequest.CachePolicy = .useProtocolCachePolicy
	internal var timeoutInterval: TimeInterval = 20
	internal var networkServiceType: URLRequest.NetworkServiceType = .default
	internal var allowsCellularAccess: Bool = true
	internal var httpShouldHandleCookies: Bool = true
	internal var httpShouldUsePipelining: Bool = true
	
	internal var description: String {
		return """
		RequestBuilder(
		method: \(method),
		url: \(url),
		headers:
		\(headers.map { "\($0.key): \($0.value)" }.joined(separator: "\n\t\t"))
		body: \((bodyEncoder as? URLBodyEncoder)?.parameters.description ?? "nil")
		)
		"""
	}
	
	
	func appendHeader(_ key: String, value: String) {
		if headers.keys.contains(key) {
			headers[key]!.append(value)
		}
		else {
			headers[key] = [value]
		}
	}
	
	func setHeader(_ key: String, value: String) {
		headers[key] = [value]
	}
}








//MARK: - Response

internal final class Response<T> {
	
	internal let statusCode: Int
	internal let headers: HttpHeaders
	internal let result: Result<T, ApiError>
	
	init(httpResponse: HTTPURLResponse, result: Result<T, ApiError>) {
		statusCode = httpResponse.statusCode
		headers = httpResponse.allHeaderFields as? HttpHeaders ?? HttpHeaders()
		self.result = result
	}
}






//MARK: - Api client

public enum ApiClientLoggingLevel {
	case none // Doesnt log at all
	case light // Just URL, status, time, http method
	case medium // url + the response body data
	case verbose // body + headers
}

internal protocol ApiClient {
	
	var baseUrl: String { get }
	var interceptors: [(RequestBuilder) -> ()] { get set }
	var loggingLevel: ApiClientLoggingLevel { get set }
}

internal extension ApiClient {
	
	func request <D: Decoder> (decoder: D, buildRequest: (inout RequestBuilder) -> ()) -> Request<D.Out> {
		var builder = RequestBuilder()
		buildRequest(&builder)
		if !builder.url.contains("http") {
			if builder.url.first == "/" {
				builder.url = baseUrl + builder.url
			}
			else {
				builder.url = baseUrl + "/" + builder.url
			}
		}
		
		// Needs to be called before urlRequest so url will be correct if httpBody and method is head, get or delete
		let requestBodyData = try? builder.bodyEncoder?.encode(request: builder)
		
		for interceptor in interceptors {
			interceptor(builder)
		}
		
		var urlRequest = URLRequest(url: URL(string: builder.url)!, cachePolicy: builder.cachePolicy, timeoutInterval: builder.timeoutInterval)
		urlRequest.httpMethod = builder.method.rawValue
		urlRequest.allHTTPHeaderFields = builder.headers.mapValues({ $0.joined(separator: ",") })
		urlRequest.httpBody = requestBodyData
		urlRequest.networkServiceType = builder.networkServiceType
		urlRequest.allowsCellularAccess = builder.allowsCellularAccess
		urlRequest.httpShouldHandleCookies = builder.httpShouldHandleCookies
		urlRequest.httpShouldUsePipelining = builder.httpShouldUsePipelining
		
		let request = Request<D.Out>()
		
		request.task = URLSession.shared.dataTask(with: urlRequest) { (httpData, httpResponse, httpError) in
			defer {
				DispatchQueue.main.async {
					request.onComplete?()
				}
			}
			
			let timeInterval = Date().timeIntervalSince(request.startDate!)
			
			if let error = httpError {
				DispatchQueue.main.async {
                    if let urlError = error as? URLError, urlError.errorCode == NSURLErrorTimedOut {
                        request.onError?(ApiError.timeout(causeBy: error))
                    }
                    else {
                        request.onError?(ApiError.unknown(causeBy: error))
                    }
                }
			}
			else if let httpResponse = httpResponse as? HTTPURLResponse, let httpData = httpData {
				self.log(response: httpResponse, data: httpData, timeInterval: timeInterval)
				
				let response: Response<D.Out>
				
				switch httpResponse.statusCode {
				case 200..<300:
					do {
						let body = try decoder.decode(data: httpData)
						response = Response(httpResponse: httpResponse, result: .success(body))
					}
					catch {
                        print("error \(error)")
						response = Response(httpResponse: httpResponse, result: .failure(ApiError.serialize(causeBy: error)))
					}
				case 400..<500:
                    response = Response(httpResponse: httpResponse, result: .failure(ApiError.badRequest(response: httpResponse, data: httpData)))
                case 500..<600:
					response = Response(httpResponse: httpResponse, result: .failure(ApiError.serverError(response: httpResponse, data: httpData)))
				default:
					response = Response(httpResponse: httpResponse, result: .failure(ApiError.unknown(causeBy: httpError)))
				}
				
				DispatchQueue.main.async {
					request.onResponse?(response)
				}
			}
			else {
				assertionFailure()
			}
		}
		
		log(request: urlRequest)
		
		return request
	}
	
	
	private func log(request: URLRequest) {
		guard loggingLevel != .none else {
			return
		}
		
		if loggingLevel != .light {
			print() // To separate logs when logging several lines
		}
		
		print("<--", request.httpMethod ?? "", request.url?.absoluteString ?? "")
		
		if loggingLevel == .verbose, let headers = request.allHTTPHeaderFields, !headers.isEmpty {
			headers.forEach { print("\($0): \($1)") }
		}
		
		if [.medium, .verbose].contains(loggingLevel), let data = request.httpBody, let string = String(data: data, encoding: .utf8) {
			print(string)
		}
	}
	
	
	private func log(response: HTTPURLResponse, data: Data?, timeInterval: TimeInterval) {
		guard loggingLevel != .none else {
			return
		}
		
		if loggingLevel != .light {
			print() // To separate logs when logging several lines
		}
		
		print("-->", response.statusCode, response.url?.absoluteString ?? "", "[\(data?.count ?? 0) b]", String(format: "[%.03f s]", timeInterval))
		
		if loggingLevel == .verbose, !response.allHeaderFields.isEmpty {
			response.allHeaderFields.forEach { print("\($0): \($1)") }
		}
		
		if [.medium, .verbose].contains(loggingLevel), let data = data {
			if loggingLevel == .medium && data.count > 10_000 {
				print("Data too large to print")
			}
			else if let string = String(data: data, encoding: .utf8) {
				print(string)
			}
		}
	}
}




//MARK: - Impl

internal final class VirtualStoresApiClient: ApiClient {
	
	internal let baseUrl: String
	internal var interceptors: [(RequestBuilder) -> ()]
	internal var loggingLevel: ApiClientLoggingLevel = .medium
	
	init(connection: ServerConnection) {
		baseUrl = connection.serverUrl
		interceptors = [{
			$0.appendHeader("APIKey", value: connection.apiKey)
		}]
	}
}
