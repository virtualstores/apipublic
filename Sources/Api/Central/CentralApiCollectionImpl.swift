//
//  CentralApiCollectionImpl.swift
//  api
//
//  Created by Carl-Johan Dahlman on 2019-10-14.
//

import Foundation
import Domain


class CentralApiCollectionImpl: CentralApiCollection {
	
	internal let apiClient: VirtualStoresApiClient
    
    public let clientId: Int64
	
    init(connection: ServerConnection, clientId: Int64) {
        self.clientId = clientId
		apiClient = VirtualStoresApiClient(connection: connection)
	}
    
    
    var centralApi: CentralApi {
		return CentralApiImpl(apiClient: apiClient, clientId: clientId)
    }
}
