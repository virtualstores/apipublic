//
//  MapZoneParser.swift
//  Api
//
//  Created by Emil Bond on 2020-12-08.
//  Copyright © 2020 Virtual Stores. All rights reserved.
//

import Foundation
import CoreGraphics
import Domain

private typealias PolygonJson = Dictionary<String, AnyObject>

public class MapZoneParser: NSObject {
    public static func getMapZonesData(fromJsonData data: Data) -> [MapZone] {
        let json = try! JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments) as! PolygonJson
        var output: [MapZone] = []
        let store = Array(json)[0].value as! NSDictionary
        let data = store["data"] as! NSDictionary
        let features = data["features"] as! [AnyObject]
        for feature in features {
            if feature != nil {
                let properties = feature["properties"] as! NSDictionary
                if let id = properties["id"] {
                    let geometry = feature["geometry"] as! NSDictionary
                    let cordinates = geometry["coordinates"] as! [[[Double]]]
                    let points: [CGPoint] = cordinates[0].map { (coords) -> CGPoint in
                        return CGPoint(x: coords[0], y: coords[1])
                    }
                    let name = properties["name"] ?? ""
                    output.append(MapZone(id: Int(id as! String) ?? -1, name: name as! String, zone: points))
                }
            }
        }
        return output
    }
}
