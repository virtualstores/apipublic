//
//  CentralApi.swift
//  Alamofire
//
//  Created by Carl-Johan Dahlman on 2019-10-14.
//

import Foundation
import Domain

internal struct MinimumVersion: Codable {
    var minimumIosVersion: String
}

internal struct GetClientsResponse: Codable {
    let clients: [ClientResponse]
}

internal struct GetStoresResponse: Codable {
    let stores: [StoreResponse]
}

internal struct ClientResponse: Codable {
    let id: Int64
    let name: String
    let stores: [StoreResponse]
}

internal struct StoreResponse: Codable {
    let id: Int64
    let name: String
    let longitude: Double?
    let latitude: Double?
    let active: Bool
    let supportedVersionCode: String?
    let address: Store.Address
    let serverConnection: ServerConnectionResponse?
    let rtlsOptions: Store.RTLS
    let startScanLocations: [PositionedCodeResponse]
    let stopScanLocations: [PositionedCodeResponse]
    let minIosVersion: String
    //let supportedPayMethods: [Store.PaymentMethod]
}

internal struct PositionedCodeResponse: Codable {
    let code: String
    let x: Double
    let y: Double
    let direction: Double
    
    var asPositionedCode: PositionedCode {
        PositionedCode(code: code, coordinates: .init(x: x, y: y), direction: direction)
    }
}

internal struct ServerConnectionResponse: Codable {
    let apiKey: String
    let serverAddress: String
    let mqttAddress: String
}

class CentralApiImpl: CentralApi {
    let apiClient: VirtualStoresApiClient
    let clientId: Int64
    
    init(apiClient: VirtualStoresApiClient, clientId: Int64) {
        self.clientId = clientId
        self.apiClient = apiClient
    }
    
    init(connection: ServerConnection, clientId: Int64) {
        self.clientId = clientId
        apiClient = .init(connection: connection)
    }
    
    func getStores(completion: @escaping (Result<[Store], Error>) -> Void) {
        apiClient.request(decoder: JsonDecoder(decodable: GetStoresResponse.self)) {
            $0.method = .get
            $0.url = "/api/v1/clients/\(clientId)/stores"
        }.resume {
            $0.onResponse { (response) in
                switch response.result {
                case .success(let response):
                    let stores: [Store] = response.stores.map { (store: StoreResponse) -> Store in
                        let startCodes = store.startScanLocations.map { $0.asPositionedCode }
                        let stopCodes = store.stopScanLocations.map { $0.asPositionedCode }
                        
                        //let startCodes: [PositionedCode] = []
                        //let stopCodes: [PositionedCode] = []
                        
                        return Store(
                            id: store.id,
                            name: store.name,
                            address: store.address,
                            latitude: store.latitude ?? 0.0,
                            longitude: store.longitude ?? 0.0,
                            active:  store.active,
                            hasMap: store.rtlsOptions.mapBoxUrl?.isEmpty == false,
                            startCodes: startCodes,
                            stopCodes: stopCodes,
                            connection: .init(
                                id: 0,
                                storeId: 1,
                                serverUrl: store.serverConnection?.serverAddress ?? "",
                                mqttUrl: store.serverConnection?.mqttAddress ?? "",
                                apiKey: store.serverConnection?.apiKey ?? ""
                            ),
                            rtls: store.rtlsOptions,
                            paymentMethods: [],
                            minVersion: store.minIosVersion
                        )
                    }
                    completion(.success(stores))
                case .failure(let error):
                    completion(.failure(error))
                }
            }
            
            $0.onError { (error) in
                completion(.failure(error))
            }
        }
        
    }
    
    func verifyCustomer(username: String, password: String, clientId: Int64, completion: @escaping (Result<User, Error>) -> Void) {
        guard let querySafeUsername = username.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed), let querySafePassword = password.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else {
            completion(.failure(ApiError.serialize(causeBy: nil)))
            return
        }
        
        apiClient.request(decoder: JsonDecoder(decodable: User.self)) {
            $0.method = .get
            $0.url = "/api/v1/customers/verify?username=\(querySafeUsername)&password=\(querySafePassword)&clientId=\(clientId)"
        }.resume {
            $0.onResponse { (response) in
                switch response.result {
                case .success(let user):
                    completion(.success(user))
                case .failure(let error):
                    completion(.failure(error))
                }
            }
            
            $0.onError { (error) in
                completion(.failure(error))
            }
        }
    }
    
    func verifyEmployee(clientId: Int64, employeeIdentifier: String, completion: @escaping (Result<Employee, Error>) -> Void) {
        guard let querySafeEmployeeIdentifier = employeeIdentifier.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else {
            completion(.failure(ApiError.serialize(causeBy: nil)))
            return
        }
        
        apiClient.request(decoder: JsonDecoder(decodable: Employee.self)) {
            $0.method = .get
            $0.url = "/api/v1/employee?clientid=\(clientId)&employeeidentifier=\(querySafeEmployeeIdentifier)"
        }.resume {
            $0.onResponse { (response) in
                switch response.result {
                case .success(let employee):
                    completion(.success(employee))
                case .failure(let error):
                    completion(.failure(error))
                }
            }
            
            $0.onError { (error) in
                completion(.failure(error))
            }
        }
    }
    
    private func loadData(from url: URL, completion: @escaping (Result<Data, Error>) -> Void) {
        let task = URLSession.shared.dataTask(with: url) { data, response, error in
            if let data = data {
                completion(.success(data))
            }
            else if let error = error {
                completion(.failure(error))
            }
            else {
                completion(.failure(ApiError.unknown(causeBy: nil)))
            }
        }
        
        task.resume()
    }
    
    func getNavigationSpace(for id: Int64, completion: @escaping (Result<NavigationSpace, Error>) -> Void) {
        getStores { result in
            switch result {
            case .success(let stores):
                if let store = stores.first(where: { $0.id == id }), let mapUrl = URL(string: store.rtls.mapBoxUrl ?? ""), let mapfenceUrl = URL(string: store.rtls.mapFenceUrl ?? ""), let navgraphUrl = URL(string: store.rtls.navGraphUrl ?? "") {
                    let dispatchGroup = DispatchGroup()
                    var mapfence: Data?
                    var navgraph: Data?
                    var offsetZones: Data?
                    var mapZones: [MapZone] = []
                    var savedError: Error?
                    
                    dispatchGroup.enter()
                    self.loadData(from: mapfenceUrl) { result in
                        switch result {
                        case .success(let data):
                            mapfence = data
                        case .failure(let error):
                            savedError = error
                        }
                        dispatchGroup.leave()
                    }
                    
                    dispatchGroup.enter()
                    self.loadData(from: navgraphUrl) { result in
                        switch result {
                        case .success(let data):
                            navgraph = data
                        case .failure(let error):
                            savedError = error
                        }
                        dispatchGroup.leave()
                    }
                    
                    if let mapZoneUrl = URL(string: store.rtls.mapZonesUrl ?? "") {
                        dispatchGroup.enter()
                        self.loadData(from: mapZoneUrl) { result in
                            switch result {
                            case .success(let data):
                                mapZones = MapZoneParser.getMapZonesData(fromJsonData: data)
                            case .failure(let error):
                                savedError = error
                            }
                            dispatchGroup.leave()
                        }
                    }
                    
                    
                    if let offsetZoneUrl = URL(string: store.rtls.mapZonesUrl ?? "") {
                        dispatchGroup.enter()
                        self.loadData(from: offsetZoneUrl) { result in
                            switch result {
                            case .success(let data):
                                offsetZones = data
                            case .failure(let error):
                                savedError = error
                            }
                            dispatchGroup.leave()
                        }
                    }
                    
                    dispatchGroup.wait()
                    
                    if let error = savedError {
                        completion(.failure(error))
                    }
                    else if let mapfence = mapfence, let navgraph = navgraph {
                        let rtls = store.rtls
                        let navigation = NavigationImpl(storeId: store.id, shelfApi: ShelfApiImpl(apiClient: self.apiClient), mapApi: MapApiImpl(apiClient: self.apiClient), rtls: rtls)
                        
                        navigation.initate(clientId: self.clientId) { error in
                            if let error = error {
                                completion(.failure(error))
                            }
                            else {
                                completion(.success(NavigationSpace(id: store.id, name: store.name, mapUrl: mapUrl, mapfence: mapfence, navgraph: navgraph, offsetZones: offsetZones, mapZones: mapZones, width: Double(rtls.width), height: Double(rtls.height), startCodes: store.startCodes, stopCodes: store.stopCodes, navigation: navigation)))
                            }
                        }
                    }
                }
                else {
                    completion(.failure(ApiError.serialize(causeBy: nil)))
                }
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    
    func getItemPosition(storeId: Int64, itemId: String, completion: @escaping (Result<[BarcodePosition], Error>) -> Void) {
        apiClient.request(decoder: JsonDecoder(decodable: [BarcodePosition].self)) {
            $0.method = .get
            $0.url = "/api/v1/shelfgroups/barcodes?storeId=\(storeId)&barcode=\(itemId)"
        }.resume {
            $0.onResponse { (response) in
                switch response.result {
                case .success(let itemPosition):
                    completion(.success(itemPosition))
                case .failure(let error):
                    completion(.failure(error))
                }
            }
            
            $0.onError { (error) in
                completion(.failure(error))
            }
        }
    }
    
    func postSessionActivate(storeId: Int64, sessionId: String, device: DeviceInformation, completion: @escaping (Error?) -> Void) {
        apiClient.request(decoder: JsonDecoder(type: Any.self)) {
            $0.method = .post
            $0.url = "/api/v1/stores/\(storeId)/sdksessions/activate"
            $0.bodyEncoder = JsonBodyEncoder(with: [
                "sessionId": sessionId,
                "device": [
                    "id": device.id,
                    "operatingSystem": device.operatingSystem,
                    "osVersion": device.osVersion,
                    "appVersion": device.appVersion,
                    "deviceModel": device.deviceModel
                ]
            ])
        }.resume {
            $0.onResponse { (response) in
                completion(nil)
            }
            
            $0.onError { (error) in
                completion(error)
            }
        }
    }
    
    func postOrders(storeId: Int64, orderIds: [String], device: DeviceInformation, completion: @escaping (Error?) -> Void){
        apiClient.request(decoder: JsonDecoder(type: Any.self)) {
            $0.method = .post
            $0.url = "/api/v1/orders"
            $0.bodyEncoder = JsonBodyEncoder(with: [
                "storeId": storeId,
                "orderIds": orderIds,
                "device":[
                    "id": device.id,
                    "operatingSystem": device.operatingSystem,
                    "osVersion": device.osVersion,
                    "appVersion": device.appVersion,
                    "deviceModel": device.deviceModel
                ]
            ])
        }.resume {
            $0.onResponse { (response) in
                completion(nil)
            }
            
            $0.onError { (error) in
                completion(error)
            }
        }
    }
}
