
import Foundation
import Domain


public class MapApiImpl: MapApi {
    internal let apiClient: VirtualStoresApiClient
    
    internal init(apiClient: VirtualStoresApiClient) {
        self.apiClient = apiClient
    }
    
    public func getBy(clientId: Int64, storeId: Int64, completion: @escaping (Result<Map, Error>) -> Void) {
        apiClient.request(decoder: JsonDecoder(decodable: GetStoresResponse.self)) {
            $0.method = .get
            $0.url = "/api/v1/clients/\(clientId)/stores"
        }.resume {
            $0.onResponse { (response) in
                switch response.result {
                case .success(let response):
                    let stores: [Store] = response.stores.map { (store: StoreResponse) -> Store in
                        let startCodes = store.startScanLocations.map { $0.asPositionedCode }
                        let stopCodes = store.stopScanLocations.map { $0.asPositionedCode }
                        return Store(
                            id: store.id,
                            name: store.name,
                            address: store.address,
                            latitude: store.latitude ?? 0.0,
                            longitude: store.longitude ?? 0.0,
                            active: store.active,
                            hasMap: store.rtlsOptions.mapBoxUrl?.isEmpty == false,
                            startCodes: startCodes,
                            stopCodes: stopCodes,
                            connection: .init(
                                id: 0,
                                storeId: 1,
                                serverUrl: store.serverConnection?.serverAddress ?? "",
                                mqttUrl: store.serverConnection?.mqttAddress ?? "",
                                apiKey: store.serverConnection?.apiKey ?? ""
                            ),
                            rtls: store.rtlsOptions,
                            paymentMethods: [],
                            minVersion: store.minIosVersion
                        )
                    }
                    if let store = stores.first(where: { $0.id == storeId }) {
                        completion(.success(Map(id: store.id, mapURL: store.rtls.mapBoxUrl ?? "", storeId: store.id, railScale: 0, pixelOffsetX: Int(store.rtls.startOffsetY), pixelOffsetY: Int(store.rtls.startOffsetY), pixelWidth: Int(store.rtls.width), pixelHeight: Int(store.rtls.height))))
                    }
                case .failure(let error):
                    completion(.failure(error))
                }
            }
            
            $0.onError { (error) in
                completion(.failure(error))
            }
        }
    }
}
