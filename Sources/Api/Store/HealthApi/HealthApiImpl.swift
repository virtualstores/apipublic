//
//  HealthApiImpl.swift
//  Api
//
//  Created by Jesper Lundqvist on 2020-06-02.
//  Copyright © 2020 Virtual Stores. All rights reserved.
//

import Domain

class HealthApiImpl: HealthApi {
    internal let apiClient: VirtualStoresApiClient
    
    internal init(apiClient: VirtualStoresApiClient) {
        self.apiClient = apiClient
    }
    
    func health(completion: @escaping (Result<Health, Error>) -> Void) {
        apiClient.request(decoder: JsonDecoder(decodable: Health.self)) {
            $0.method = .get
            $0.url = "/api/v2/health"
            
        }.resume {
            $0.onResponse { (response) in
                switch response.result {
                case .success(let health):
                    completion(.success(health))
                case .failure(let error):
                    completion(.failure(error))
                }
            }
            
            $0.onError { (error) in
                completion(.failure(error))
            }
        }
    }
}
