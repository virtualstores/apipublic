//
//  RequestExtensions.swift
//  Api
//
//  Created by Carl-Johan Dahlman on 2019-10-28.
//

import Foundation


public extension CreateShoppingSessionRequest {
    static func toParamters(_ request: CreateShoppingSessionRequest) -> [String: Any] {
        return [
            "identificationNumber" : request.identificationNumber as Any,
            "createCart" : request.createCart as Any,
            "deviceCount": request.deviceCount as Any,
            "terminalId" : request.terminalId as Any,
            
        ]
    }
}

public extension AddItemToCartRequest {
    static func toParamters(_ request: AddItemToCartRequest) -> [String: Any] {
        return [
            "deviceId" : request.deviceId ?? "" as Any,
            "barcode" : request.barcode as Any,
            "quantity": request.quantity ?? 0 as Any
            
        ]
    }
}

public extension ChangeItemQuantityInCartRequest {
    static func toParameters(_ request: ChangeItemQuantityInCartRequest) -> [String: Any] {
        return [
            "deviceId" : request.deviceId as Any,
            "barcode" : request.barcode as Any,
            "receiptItemId" : request.receiptItemId as Any,
            "quantity" : request.quantity as Any
        ]
    }
}

public extension CreateCartRequest {
    static func toParameters(_ request: CreateCartRequest) -> [String: Any] {
        return [
            "receiptlabel": request.receiptlabel as Any
        ]
    }
}
