import Foundation
import Domain


internal struct PayExSwishTransaction: Decodable {
    let id: String
    let operations: [PayExOperation]
}

internal struct PayExSwishSale: Decodable {
    let id: String
    let transaction: PayExSwishTransaction
}

internal struct PayExSwishStart: Decodable {
    let payment: String
    let sale: PayExSwishSale
}

public class PayExSwishPaymentsApiImpl: PayExSwishPaymentsApi {
    
    internal let apiClient: VirtualStoresApiClient
    
    internal init(apiClient: VirtualStoresApiClient) {
        self.apiClient = apiClient
    }
    
    public func start(sessionId: Int64, cartId: String, completion: @escaping (Result<PayExSwishStartResponse, Error>) -> Void) {
        apiClient.request(decoder: JsonDecoder(decodable: PayExSwishStart.self)) {
            $0.method = .post
            $0.url = "/api/payex/swishpayments/sessions/\(sessionId)/carts/\(cartId.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed)!)/startpurchase"
        }.resume {
            $0.onResponse { (response) in
				switch response.result {
				case .success(let payex):
                    guard let operation = payex.sale.transaction.operations.first(where: { $0.rel == "redirect-app-swish" }) else {
                        return completion(.failure(ApiError.serialize(causeBy: nil)))
                    }
                    
                    let response = PayExSwishStartResponse(paymentId: payex.payment, transactionId: payex.sale.transaction.id, operation: operation)
					completion(.success(response))
				case .failure(let error):
					completion(.failure(error))
				}
            }
            
            $0.onError { (error) in
                completion(.failure(error))
            }
        }
    }
    
    public func verify(sessionId: Int64, cartId: String, completion: @escaping (Error?) -> Void) {
        apiClient.request(decoder: StringDecoder()) {
            $0.method = .get
            $0.url = "/api/payex/swishpayments/sessions/\(sessionId)/carts/\(cartId.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed)!)/verify"
        }.resume {
            $0.onResponse { response in
                if response.statusCode == 200 || response.statusCode == 202 {
                    completion(nil)
                }
                else {
                    switch response.result {
                    case .success:
                        completion(ApiError.unknown(causeBy: nil))
                    case .failure(let error):
                        completion(error)
                    }
                }
            }
            
            $0.onError { (error) in
                completion(error)
            }
        }
    }
    
    public func callback(sessionId: Int64, cartId: String, paymentId: String, completion: @escaping (Error?) -> Void) {
        apiClient.request(decoder: JsonDecoder(type: Any.self)) {
            $0.method = .post
            $0.url = "/api/payex/swishpayments/callback?sessionId=\(sessionId)&cartId=\(cartId.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)"
            $0.bodyEncoder = JsonBodyEncoder(with: [
                "payment" : [
                    "id": paymentId
                ]
            ])
        }.resume {
            $0.onResponse { response in
                if response.statusCode == 200 {
                    completion(nil)
                }
                else {
                    switch response.result {
                    case .success:
                        completion(ApiError.unknown(causeBy: nil))
                    case .failure(let error):
                        completion(error)
                    }
                }
            }
            
            $0.onError { (error) in
                completion(error)
            }
        }
    }
}
