import Foundation

public struct PayExOperationDto : Codable {
    
    public var href: String?
    public var rel: String?
    public var method: String?
    public var contentType: String?
    
    public init (href: String?, rel: String?, method: String?, contentType: String?){
        self.href = href
        self.rel = rel
        self.method = method
        self.contentType = contentType
    }
    
    public enum CodingKeys: String, CodingKey {
        case href
        case rel
        case method
        case contentType
    }
}
