import Foundation
import Domain


public class PayExCardPaymentsApiImpl: PayExCardPaymentsApi {
    
    internal let apiClient: VirtualStoresApiClient
    
    internal init(apiClient: VirtualStoresApiClient) {
        self.apiClient = apiClient
    }
    
    public func start(sessionId: Int64, cartId: String, generateToken: Bool?, paymentToken: String?, completion: @escaping (Result<PayExStartResponse, Error>) -> Void) {
		let query: String
		if let paymentToken = paymentToken {
			query = "paymenttoken=\(paymentToken)"
		}
        else if let generateToken = generateToken {
            query = "generatetoken=\(generateToken)"
        }
		else {
			query = ""
		}
        
        apiClient.request(decoder: JsonDecoder(decodable: PayExStartResponse.self)) {
            $0.method = .post
            $0.url = "/api/payex/cardpayments/sessions/\(sessionId)/carts/\(cartId.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed)!)/startpurchase\(query.isEmpty ? "" : "?" + query)"
        }.resume {
            $0.onResponse { (response) in
				completion(response.result.mapError { $0 as Error })
            }
            
            $0.onError { (error) in
                completion(.failure(error))
            }
        }
    }
    
    public func verify(sessionId: Int64, cartId: String, completion: @escaping (Result<PayExVerification, Error>) -> Void) {
        apiClient.request(decoder: StringDecoder()) {
            $0.method = .get
            $0.url = "/api/payex/cardpayments/sessions/\(sessionId)/carts/\(cartId.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed)!)/verify"
        }.resume {
            $0.onResponse { (response) in
                switch response.result {
                case .success(let responseString):
                    if response.statusCode == 202 {
                        completion(.success(.init(token: nil, maskedCardNumber: nil, cardBrand: nil)))
                    }
                    else {
                        let jsonDecoder = JsonDecoder(decodable: PayExVerification.self)
                        do {
                            guard let data = responseString.data(using: .utf8) else {
                                throw DecodingError.dataCorrupted(DecodingError.Context(codingPath: [], debugDescription: "Invalid encoding"))
                            }
                            let verification = try jsonDecoder.decode(data: data)
                            completion(.success(verification))
                        }
                        catch let decodingError {
                            completion(.failure(decodingError))
                        }
                    }
                case .failure(let error):
                    completion(.failure(error))
                }
            }
            
            $0.onError { (error) in
                completion(.failure(error))
            }
        }
    }
	
	public func payexCallback(sessionId: Int64, cartId: String, paymentId: String, completion: @escaping (Result<Any, Error>) -> Void) {
		apiClient.request(decoder: JsonDecoder(type: Any.self)) {
			$0.method = .post
			$0.url = "payex/callback?sessionid=\(sessionId)&cartid=\(cartId.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)"
			$0.bodyEncoder = JsonBodyEncoder(with: [
				"payment" : [ "id" : paymentId]
			])
		}.resume {
			$0.onResponse { (response) in
                if response.statusCode == 200 {
                    completion(.success(response))
                }
                else {
                    completion(response.result.mapError{ $0 as Error })
                }
            }
            
            $0.onError { (error) in
                completion(.failure(error))
            }
		}
	}
    
    public func complete(sessionId: Int64, cartId: String, completion: @escaping (Result<Any, Error>) -> Void) {
        apiClient.request(decoder: JsonDecoder(type: Any.self)) {
            $0.method = .put
            $0.url = "/api/payex/cardpayments/sessions/\(sessionId)/carts/\(cartId.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed)!)/completepurchase"
        }.resume {
            $0.onResponse { (response) in
                if response.statusCode == 200 {
                    completion(.success(response))
                }
                else {
                    completion(response.result.mapError{ $0 as Error })
                }
            }
            
            $0.onError { (error) in
                completion(.failure(error))
            }
        }
    }
    
    public func cancel(sessionId: Int64, cartId: String, completion: @escaping (Result<Any, Error>) -> Void) {
        apiClient.request(decoder: JsonDecoder(type: Any.self)) {
            $0.method = .put
            $0.url = "/api/payex/cardpayments/sessions/\(sessionId)/carts/\(cartId.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed)!)/cancelpurchase"
        }.resume {
            $0.onResponse { (response) in
                completion(response.result.mapError{ $0 as Error })
            }
            
            $0.onError { (error) in
                completion(.failure(error))
            }
        }
    }
}
