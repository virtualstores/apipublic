import Foundation
import Domain

public class KlarnaPaymentsApiImpl: KlarnaPaymentsApi {
    
    internal let apiClient: VirtualStoresApiClient
    
    internal init(apiClient: VirtualStoresApiClient) {
        self.apiClient = apiClient
    }
    
    public func start(sessionId: Int64, cartId: String, completion: @escaping (Result<KlarnaPaymentSession, Error>) -> Void) {
        apiClient.request(decoder: JsonDecoder(decodable: KlarnaPaymentSessionDto.self)) {
            $0.method = .get
            $0.url = "/api/klarna/payments/sessions/\(sessionId)/carts/\(cartId.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed)!)/paymentsession"
        }.resume {
            $0.onResponse { (response) in
                completion(response.result.map { KlarnaPaymentSessionDto.toKlarnaPaymentSession($0) }.mapError { $0 as Error })
            }
            
            $0.onError { (error) in
                completion(.failure(error))
            }
        }
    }
    
    public func complete(sessionId: Int64, cartId: String, authToken: String, completion: @escaping (Result<Any, Error>) -> Void) {
        apiClient.request(decoder: JsonDecoder(type: Any.self)) {
            $0.method = .get
            $0.url = "/api/klarna/payments/sessions/\(sessionId)/carts/\(cartId.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed)!)/complete"
        }.resume {
            $0.onResponse { (response) in
                completion(response.result.mapError{ $0 as Error })
            }
            
            $0.onError { (error) in
                completion(.failure(error))
            }
        }
    }
    
    public func cancel(sessionId: Int64, cartId: String, completion: @escaping (Result<Any, Error>) -> Void) {
        apiClient.request(decoder: JsonDecoder(type: Any.self)) {
            $0.method = .get
            $0.url = "/api/klarna/payments/sessions/\(sessionId)/carts/\(cartId.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed)!)/cancel"
        }.resume {
            $0.onResponse { (response) in
                completion(response.result.mapError{ $0 as Error })
            }
            
            $0.onError { (error) in
                completion(.failure(error))
            }
        }
    }
    
}
