import Foundation

public class KlarnaPaymentSessionDto: Codable {
    
    public var sessionId: String?
    public var clientToken: String?
    public var paymentMethodCategories: [PaymentMethodCategoryDto]?
    
    public init(sessionId: String?, clientToken: String?, paymentMethodCategories: [PaymentMethodCategoryDto]?) {
        self.sessionId = sessionId
        self.clientToken = clientToken
        self.paymentMethodCategories = paymentMethodCategories
    }
    
    public struct PaymentMethodCategoryDto: Codable {
        public var identifier: String?
        public var name: String?
        public var assetUrls: AssetUrlsDto?
        
        public init(identifier: String?, name: String?, assetUrls: AssetUrlsDto?) {
            self.identifier = identifier
            self.name = name
            self.assetUrls = assetUrls
        }
        
        public enum CodingKeys: String, CodingKey {
            case identifier
            case name
            case assetUrls
        }
    }
    
    public struct AssetUrlsDto: Codable {
        public var descriptive: String?
        public var standard: String?
        
        public init(descriptive: String?, standard: String?){
            self.descriptive = descriptive
            self.standard = standard
        }
        
        public enum CodingKeys: String, CodingKey {
            case descriptive
            case standard
        }
    }
    
    public enum CodingKeys: String, CodingKey {
        case sessionId
        case clientToken
        case paymentMethodCategories
    }
}
