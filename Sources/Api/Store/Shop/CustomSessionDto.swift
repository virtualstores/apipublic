//
//  CustomSessionDto.swift
//  Alamofire
//
//  Created by Carl-Johan Dahlman on 2019-10-29.
//

import Foundation

public struct CustomSessionDto: Codable {

    public enum State: Int, Codable {
        case _0 = 0
        case _1 = 1
        case _2 = 2
        case _3 = 3
        case _4 = 4
        case _5 = 5
        case _6 = 6
        case _7 = 7
        case _8 = 8
    }
    public var _id: Int64?

    public var userId: Int64?

    public var state: State?

    public var externalVisitId: Int64?

    public var referenceCode: String?

    public var controlLevel: Int?

    public init(_id: Int64?, userId: Int64?, state: State?, deviceId: String?, externalVisitId: Int64?, referenceCode: String?,controlLevel: Int?, dateCreated: Date?) {
        self._id = _id
        self.userId = userId
        self.state = state
        self.externalVisitId = externalVisitId
        self.referenceCode = referenceCode
        self.controlLevel = controlLevel
     
    }
    public enum CodingKeys: String, CodingKey {
        case _id = "id"
        case userId
        case state
        case externalVisitId
        case referenceCode
    }
}

public struct SessionFromReferenceDto: Codable {
    public var id: Int64?
    public var state: Int?
    
    public init(id: Int64?, state: Int?) {
        self.id = id
        self.state = state
    }
}
