//
//  ShopApiImpl.swift
//  Api
//
//  Created by Carl-Johan Dahlman on 2019-10-25.
//

import Foundation
import Domain


public class ShopApiImpl: ShopApi {
    
    

    
    
	internal let apiClient: VirtualStoresApiClient
	
	internal init(apiClient: VirtualStoresApiClient) {
		self.apiClient = apiClient
	}
    
    public func createMobileSession(identificationNumber: String, createCart: Bool, receiptLabel: String?, completion: @escaping (Result<Session, ApiError>) -> Void) {
        let bodyEncoder: JsonBodyEncoder<[String: Any]>
        if let receiptLabel = receiptLabel {
            bodyEncoder = JsonBodyEncoder(with: [
                "identificationNumber" : identificationNumber,
                "createCart" : createCart,
                "receiptLabel": receiptLabel
            ])
        }
        else {
            bodyEncoder = JsonBodyEncoder(with: [
                "identificationNumber" : identificationNumber,
                "createCart" : createCart
            ])
        }
        
        apiClient.request(decoder: JsonDecoder(decodable: CustomSessionResponseDto.self)) {
            $0.method = .post
            $0.url = "/api/v2/shoppingsessions/mobile"
            $0.bodyEncoder = bodyEncoder
        }.resume {
            $0.onResponse { (response) in
                switch response.result {
                case .success(let dto):
                    guard let session = CustomSessionDto.toSession(dto.session) else {
                        return completion(.failure(ApiError.serialize(causeBy: nil)))
                    }
                    
                    completion(.success(session))
                    
                case .failure(let error):
                    completion(.failure(error))
                }
            }
            
            $0.onError { (error) in
                completion(.failure(error))
            }
        }
    }
    
    public func create(identificationNumber: String, createCart: Bool, completion: @escaping (Result<Session, ApiError>) -> Void) {
		apiClient.request(decoder: JsonDecoder(decodable: CustomSessionResponseDto.self)) {
			$0.method = .post
			$0.url = "/api/v2/shoppingsessions"
			$0.bodyEncoder = JsonBodyEncoder(with: [
				"identificationNumber" : identificationNumber,
				"createCart" : createCart,
				"deviceCount": 0,
				"terminalId" : 0,
			])
		}.resume {
			$0.onResponse { (response) in
				switch response.result {
				case .success(let dto):
					guard let session = CustomSessionDto.toSession(dto.session) else {
						return completion(.failure(ApiError.serialize(causeBy: nil)))
					}
					
					completion(.success(session))
					
				case .failure(let error):
					completion(.failure(error))
				}
			}
			
			$0.onError { (error) in
				completion(.failure(error))
			}
		}
    }
    
    public func activate(sessionId: Int64, completion: @escaping (ApiError?) -> Void) {
		apiClient.request(decoder: JsonDecoder(type: Any.self)) {
			$0.method = .put
			$0.url = "/api/v2/shoppingsessions/\(sessionId)/activate"
		}.resume {
			$0.onResponse { (_) in
				completion(nil)
			}
			$0.onError { (error) in
				completion(ApiError.serialize(causeBy: error))
			}
		}
    }
    
    public func getSession(sessionId: Int64, completion: @escaping (Result<Session, ApiError>) -> Void) {
        apiClient.request(decoder: JsonDecoder(decodable: CustomSessionDto.self)) {
            $0.method = .get
            $0.url = "/api/v2/shoppingsessions/\(sessionId)"
        }.resume {
            $0.onResponse { (response) in
                switch response.result {
                case .success(let data):
                    let session = CustomSessionDto.toSession(data)
                    if session != nil {
                        completion(.success(session!))
                    }
                    else {
                        completion(.failure(.serialize(causeBy: nil)))
                    }
                case .failure(let error):
                    print(error.localizedDescription)
                    completion(.failure(error))
                }
            }
            $0.onError { (error) in
                completion(.failure(error))
            }
        }
    }
    
    public func getSession(referenceCode: String, completion: @escaping (Result<Session, ApiError>) -> Void) {
        apiClient.request(decoder: JsonDecoder(decodable: SessionFromReferenceDto.self)) {
            $0.method = .get
            $0.url = "/api/v2/shoppingsessions?referenceCode=\(referenceCode)"
        }.resume {
            $0.onResponse { (response) in
                switch response.result {
                case .success(let dto):
                    guard let session = SessionFromReferenceDto.toSession(dto) else {
                        completion(.failure(ApiError.serialize(causeBy: nil)))
                        return
                    }
                    
                    completion(.success(session))
                case .failure(let error):
                    completion(.failure(error))
                }
            }
            
            $0.onError { (error) in
                completion(.failure(error))
            }
        }
    }
    
    public func addItem(sessionId: Int64, cartId: String, barcode: String, quantity: Double, completion: @escaping (Result<PosResponse, ApiError>) -> Void) {
		apiClient.request(decoder: JsonDecoder(decodable: PosResponseView.self)) {
			$0.method = .post
            $0.url = "/api/v2/shoppingsessions/\(sessionId)/carts/\(cartId.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed)!)/items"
			$0.bodyEncoder = JsonBodyEncoder(with: [
				"barcode" : barcode,
				"quantity": quantity
			])
		}.resume {
			self.setupPosRequest($0, completion: completion)
		}
    }
    
    public func updateItem(sessionId: Int64, cartId: String, cartItemId: String, barcode: String, quantity: Double, completion: @escaping (Result<PosResponse, ApiError>) -> Void) {
		apiClient.request(decoder: JsonDecoder(decodable: PosResponseView.self)) {
			$0.method = .patch
			$0.url = "/api/v2/shoppingsessions/\(sessionId)/carts/\(cartId.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed)!)/items"
			$0.bodyEncoder = JsonBodyEncoder(with: [
				"barcode" : barcode,
				"quantity": quantity,
				"receiptItemId" : cartItemId
			])
		}.resume {
			self.setupPosRequest($0, completion: completion)
		}
    }
    
    public func deleteItem(sessionId: Int64, cartId: String, cartItemId: String, barcode: String, completion: @escaping (Result<PosResponse, ApiError>) -> Void) {
		apiClient.request(decoder: JsonDecoder(decodable: PosResponseView.self)) {
			$0.method = .delete
			$0.url = "/api/v2/shoppingsessions/\(sessionId)/carts/\(cartId.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed)!)/items/\(barcode)/positemid/\(cartItemId.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed)!)"
		}.resume {
			self.setupPosRequest($0, completion: completion)
		}
    }
    
    public func addCart(sessionId: Int64, cartId: String, completion: @escaping (Result<PosResponse, ApiError>) -> Void) {
        apiClient.request(decoder: JsonDecoder(decodable: PosResponseView.self)) {
            $0.method = .post
            $0.url = "/api/v2/shoppingsessions/\(sessionId)/carts"
            $0.bodyEncoder = JsonBodyEncoder(with: [
                "receiptlabel": cartId,
            ])
        }.resume {
            self.setupPosRequest($0, completion: completion)
        }
    }
    
    public func getCarts(sessionId: Int64, completion: @escaping (Result<PosResponse, ApiError>) -> Void) {
		apiClient.request(decoder: JsonDecoder(decodable: PosResponseView.self)) {
			$0.method = .get
			$0.url = "/api/v2/shoppingsessions/\(sessionId)/carts"
		}.resume {
			self.setupPosRequest($0, completion: completion)
		}
    }
    
    public func deleteCart(sessionId: Int64, cartId: String, completion: @escaping (Result<PosResponse, ApiError>) -> Void) {
		apiClient.request(decoder: JsonDecoder(decodable: PosResponseView.self)) {
			$0.method = .delete
			$0.url = "/api/v2/shoppingsessions/\(sessionId)/carts/\(cartId.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed)!)"
		}.resume {
			self.setupPosRequest($0, completion: completion)
		}
    }
    
    public func finalize(sessionId: Int64,devicePayment: Bool, completion: @escaping (Result<PosResponse, ApiError>) -> Void) {
		apiClient.request(decoder: JsonDecoder(decodable: PosResponseView.self)) {
			$0.method = .put
			$0.url = "/api/v2/shoppingsessions/\(sessionId)/finalize"
            $0.bodyEncoder = URLBodyEncoder(parameters: [
                "devicepayment": devicePayment
            ])
		}.resume {
			self.setupPosRequest($0, completion: completion)
		}
    }
    
    public func cancel(sessionId: Int64, completion: @escaping (Result<PosResponse, ApiError>) -> Void) {
		apiClient.request(decoder: JsonDecoder(decodable: PosResponseView.self)) {
			$0.method = .put
			$0.url = "/api/v2/shoppingsessions/\(sessionId)/cancel"
		}.resume { 			self.setupPosRequest($0, completion: completion)
		}
    }
	
    public func clearControl(sessionId: Int64, completion: @escaping (ApiError?) -> Void) {
        apiClient.request(decoder: JsonDecoder(decodable: String.self)) {
            $0.method = .put
            $0.url = "/api/v2/controls/sessions/\(sessionId)/clear"
        }.resume {
           $0.onResponse { (response) in
                switch response.result {
                case .success(_):
                   completion(nil)
                case .failure(let error):
                    completion(error)
                }
            }
            $0.onError { (error) in
                completion(error)
            }
        }
    }
	
    public func getWorkplaces(referenceCode: String, completion: @escaping (Result<[Workplace], Error>) -> Void) {
        guard let urlEncodedReferenceCode = referenceCode.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else {
            return completion(.failure(ApiError.serialize(causeBy: nil)))
        }
        
        apiClient.request(decoder: JsonDecoder(decodable: [Workplace].self)) {
            $0.method = .get
            $0.url = "/api/v2/shoppingsessions/workplaces"
            $0.bodyEncoder = URLBodyEncoder(parameters: [
                "referenceCode": urlEncodedReferenceCode
            ])
        }.resume {
           $0.onResponse { (response) in
                switch response.result {
                case .success(let workplaces):
                    completion(.success(workplaces))
                case .failure(let error):
                    completion(.failure(error))
                }
            }
            $0.onError { (error) in
                completion(.failure(error))
            }
        }
    }
	
	private func setupPosRequest(_ request: Request<PosResponseView>, completion: @escaping (Result<PosResponse, ApiError>) -> Void) {
		request.onResponse { (response) in
			completion(response.result.map { PosResponseView.toPosResponse($0) })
		}
		
		request.onError { (error) in
			completion(.failure(error))
		}
	}
}
