//
//  CustomSessionResponseDto.swift
//  Alamofire
//
//  Created by Carl-Johan Dahlman on 2019-10-29.
//

import Foundation

public struct CustomSessionResponseDto: Codable {

    public var session: CustomSessionDto?
    public init(session: CustomSessionDto?) {
        self.session = session
    }

}
