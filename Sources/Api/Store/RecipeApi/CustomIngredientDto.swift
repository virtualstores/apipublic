//
//  CustomIngredientDto.swift
//  Api
//
//  Created by Gustav Larson on 2020-01-27.
//

import Foundation

public class CustomIngredientDto: Codable {
    public let name: String?
    public let unit: String?
    public let optional: Bool?
    public let quantity: [Double]?
    public let item: CustomRecipeItemDto?
    
    
    public init(name: String? = nil, unit: String? = nil, optional: Bool? = nil, quantity: [Double]? = nil, item: CustomRecipeItemDto? = nil) {
        self.name = name
        self.unit = unit
        self.optional = optional
        self.quantity = quantity
        self.item = item
    }
    
    public enum CodingKeys: String, CodingKey {
        case name
        case unit
        case optional
        case quantity
        case item
    }
}
