//
//  CustomRecipeDto.swift
//  Api
//
//  Created by Gustav Larson on 2020-01-27.
//

import Foundation

public class CustomRecipeDto: Codable {
    public let id: Int?
    public let recipeId: String?
    public let name: String?
    public let imageUrl: String?
    public let recipeUrl: String?
    public let cookingTimeInMinutes: Double?
    public let numberOfMeals: [Double]?
    public let categories: [String]?
    public let allergies: [String]?
    public let cookingInstruction: [String]?
    public let ingredientSections: [CustomIngredientSectionDto]?

    public init(id: Int? = nil, recipeId: String? = nil, name: String? = nil, imageUrl: String? = nil, recipeUrl: String? = nil, cookingTimeInMinutes: Double? = nil, numberOfMeals: [Double]? = nil, categories: [String]? = nil, allergies: [String]? = nil, cookingInstruction: [String]? = nil, ingredientSections: [CustomIngredientSectionDto]? = nil) {
        self.id = id
        self.recipeId = recipeId
        self.name = name
        self.imageUrl = imageUrl
        self.recipeUrl = recipeUrl
        self.cookingTimeInMinutes = cookingTimeInMinutes
        self.numberOfMeals = numberOfMeals
        self.categories = categories
        self.allergies = allergies
        self.cookingInstruction = cookingInstruction
        self.ingredientSections = ingredientSections
    }

    public enum CodingKeys: String, CodingKey {
        case id
        case recipeId
        case name
        case imageUrl
        case recipeUrl
        case cookingTimeInMinutes
        case numberOfMeals
        case categories
        case allergies
        case cookingInstruction
        case ingredientSections
    }
    
}
