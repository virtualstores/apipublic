//
//  CustomIngredientSectionsDto.swift
//  Api
//
//  Created by Gustav Larson on 2020-01-27.
//

import Foundation

public class CustomIngredientSectionDto: Codable {
    public let sectionName: String?
    public let ingredients: [CustomIngredientDto]?

    public init(sectionName: String? = nil, ingredients: [CustomIngredientDto]? = nil) {
        self.sectionName = sectionName
        self.ingredients = ingredients
    }
    
    public enum CodingKeys: String, CodingKey {
        case sectionName
        case ingredients
    }
}
