//
//  RecipeApiImpl.swift
//  Api
//
//  Created by Gustav Larson on 2020-01-24.
//

import Foundation
import Domain

public class RecipeApiImpl: RecipeApi {
    
    internal let apiClient: VirtualStoresApiClient
    
    internal init(apiClient: VirtualStoresApiClient) {
        self.apiClient = apiClient
    }
    
    public func getRecipes(completion: @escaping (Result<[Recipe], Error>) -> Void) {
        apiClient.request(decoder: JsonDecoder.init(decodable: [CustomRecipeDto].self)) {
            $0.method = .get
            $0.url = "/api/v2/recipes/extension"
        }.resume {
            $0.onResponse { (response) in
                completion(response.result.map { CustomRecipeDto.toRecipes(dtos: $0) }.mapError { $0 as Error })
            }
            
            $0.onError { (error) in
                completion(.failure(error))
            }
        }
    }
    
    public func getRecipesByCategory(category: String, completion: @escaping (Result<[Recipe], Error>) -> Void) {
        //TODO
    }
}
