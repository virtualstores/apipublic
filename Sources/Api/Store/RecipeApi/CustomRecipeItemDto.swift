//
//  CustomRecipeItemDto.swift
//  Api
//
//  Created by Gustav Larson on 2020-01-27.
//

import Foundation

public class CustomRecipeItemDto: Codable {
    public let barcode: String?
    public let code: String?
    public let imageUrl: String?
    public let name: String?
    public let unit: String?
    public let shelfTierIds: [Int64]?
    public let quantity: [Double]?
    
    public init(barcode: String? = nil, code: String? = nil, imageUrl: String? = nil, name: String? = nil, unit: String?, shelfTierIds: [Int64]? = nil, quantity: [Double]? = nil) {
        self.barcode = barcode
        self.code = code
        self.imageUrl = imageUrl
        self.name = name
        self.unit = unit
        self.shelfTierIds = shelfTierIds
        self.quantity = quantity
    }
    
    public enum CodingKeys: String, CodingKey {
        case barcode
        case code
        case imageUrl
        case name
        case unit
        case shelfTierIds
        case quantity
    }
}
