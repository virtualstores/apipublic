
import Foundation
import Domain


public class SearchApiImpl: SearchApi {
    
	internal let apiClient: VirtualStoresApiClient
	
	internal init(apiClient: VirtualStoresApiClient) {
		self.apiClient = apiClient
	}
	
    
    public func findBy(withExtension: Bool, searchvalue: String, onlyWithPosition: Bool?, itemGroupId: String?, limit: Int?, completion: @escaping (Swift.Result<SearchResult, Error>) -> Void) -> Cancellable {
        let url: String
        
        if withExtension {
            url = "/api/v2/items/extension/find/\(searchvalue.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed) ?? "")"
        }
        else {
            url = "/api/v2/items/find/\(searchvalue.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed) ?? "")"
        }
        
		return apiClient.request(decoder: JsonDecoder(decodable: SearchResultResponse.self)) {
			$0.method = .get
			$0.url = url
            $0.bodyEncoder = URLBodyEncoder(parameters: [
                "onlyWithPosition": onlyWithPosition ?? false,
                "itemGroupId": itemGroupId ?? "",
                "limit": limit ?? .max
            ])
		}.resume {
			$0.onResponse { (response) in
				completion(response.result.map { SearchResultResponse.toSearchResult($0) }.mapError { $0 as Error })
			}
			
			$0.onError { (error) in
				completion(.failure(error))
			}
		}
    }
}


extension Request: Cancellable {}
