//
//  MessageDto.swift
//  Api
//
//  Created by Jesper Lundqvist on 2020-06-25.
//  Copyright © 2020 Virtual Stores. All rights reserved.
//

import Foundation
import Domain

internal struct MessageCardDto: Codable {
    internal enum CardType: Int, Codable {
        case small = 0
        case big = 1
        
        func toCartType() -> Message.CardType {
            switch self {
            case .big:
                return .big
            case .small:
                return .small
            }
        }
    }
    
    let title: String
    let description: String?
    let imageUrl: String?
    let type: CardType
}

internal struct ExposureDefinitionDto: Codable {
    internal enum ExposureDefinitionType: Int, Codable {
        case wholeMap = 0
        case zones = 1
        
        func toExposureType() -> Message.ExposureType {
            switch self {
            case .wholeMap:
                return .wholeMap
            case .zones:
                return .zones
            }
        }
    }
    
    let type: ExposureDefinitionType
    let zones: [ExposureZoneDto]?
}

internal struct ExposureZoneDto: Codable {
    let id: String
    let coordinates: [[[Double]]]
}

internal struct MessageDto: Codable {
    let id: Int64
    let name: String
    let from: String
    let to: String
    let card: MessageCardDto
    let exposureDefinition: ExposureDefinitionDto
    
    func toMessage() -> Message? {
        let dateFormatter = ISO8601DateFormatter()
        dateFormatter.formatOptions = [
            .withDashSeparatorInDate,
            .withFullDate,
            .withColonSeparatorInTime
        ]
        
        // TODO: Variable modifiedToDate is temporary. Which makes it possible to include the last date of the incoming period from the server
        guard let fromDate = dateFormatter.date(from: from), let toDate = dateFormatter.date(from: to),
              let modifiedToDate = Calendar.current.date(byAdding: .day, value: 1, to: toDate) else {
            return nil
        }
        
        return Message(
            id: id,
            name: name,
            duration: DateInterval(start: fromDate, end: modifiedToDate),
            cardType: card.type.toCartType(),
            title: card.title,
            description: card.description ?? "",
            image: URL(string: card.imageUrl ?? ""),
            exposureType: exposureDefinition.type.toExposureType(),
            zones: exposureDefinition.zones?.map { zone in
                return zone.coordinates[0].map { CGPoint(x: $0[0], y: $0[1]) }
            } ?? [],
            shelf: nil
        )
    }
}
