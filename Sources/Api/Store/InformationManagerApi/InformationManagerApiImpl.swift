//
//  InformationManagerApiImpl.swift
//  Api
//
//  Created by Jesper Lundqvist on 2020-06-25.
//  Copyright © 2020 Virtual Stores. All rights reserved.
//

import Domain

class InformationManagerApiImpl: InformationManagerApi {
    internal let apiClient: VirtualStoresApiClient
    
    internal init(apiClient: VirtualStoresApiClient) {
        self.apiClient = apiClient
    }
    
    func getMessages(completion: @escaping (Result<[Message], Error>) -> Void) {
        apiClient.request(decoder: JsonDecoder(decodable: [MessageDto].self)) {
            $0.method = .get
            $0.url = "/api/v2/messages"
        }.resume {
            $0.onResponse { (response) in
                switch response.result {
                case .success(let messages):
                    completion(.success(messages.compactMap { $0.toMessage() }))
                case .failure(let error):
                    completion(.failure(error))
                }
            }
            
            $0.onError { (error) in
                completion(.failure(error))
            }
        }
    }
}
