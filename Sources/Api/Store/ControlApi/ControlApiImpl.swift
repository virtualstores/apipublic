//
//  ControlApiImpl.swift
//  Api
//
//  Created by Jesper Lundqvist on 2020-08-05.
//  Copyright © 2020 Virtual Stores. All rights reserved.
//

import Domain

class ControlApiImpl: ControlApi {
    internal let apiClient: VirtualStoresApiClient
    
    internal init(apiClient: VirtualStoresApiClient) {
        self.apiClient = apiClient
    }
    
    func getCarts(forSession sessionId: Int64, employeeId: String, completion: @escaping (Result<ControlPosResponse, Error>) -> Void) {
        apiClient.request(decoder: JsonDecoder(decodable: ControlPosResponseView.self)) {
            $0.method = .get
            $0.url = "/api/v2/controls/sessions/\(sessionId)?employeeidentifier=\(employeeId)"
        }.resume {
            $0.onResponse { (response) in
                switch response.result {
                case .success(let response):
                    completion(.success(ControlPosResponseView.toControlPosResponse(response)))
                case .failure(let error):
                    completion(.failure(error))
                }
            }
            
            $0.onError { (error) in
                completion(.failure(error))
            }
        }
    }
    
    func reportCart(forSession sessionId: Int64, forceSuccess: Bool, completion: @escaping (Error?) -> Void) {
        apiClient.request(decoder: JsonDecoder(decodable: String.self)) {
            $0.method = .put
            $0.url = "/api/v2/controls/sessions/\(sessionId)/report?forcesuccess=\(forceSuccess)"
        }.resume  {
            $0.onResponse { (response) in
                switch response.result {
                case .success(_):
                    completion(nil)
                case .failure(let error):
                    completion(error)
                }
            }
        }
    }
    
    func addItem(forSession sessionId: Int64, cartId: String, barcode: String, quantity: Double, completion: @escaping (Result<ControlPosResponse, Error>) -> Void) {
        guard let querySafeCartId = cartId.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed) else {
            completion(.failure(ApiError.unknown(causeBy: nil)))
            return
        }
        
        apiClient.request(decoder: JsonDecoder(decodable: ControlPosResponseView.self)) {
            $0.method = .post
            $0.url = "/api/v2/controls/sessions/\(sessionId)/carts/\(querySafeCartId)/items"
            $0.bodyEncoder = JsonBodyEncoder(with: [
                "barcode" : barcode,
                "quantity": quantity
            ])
        }.resume {
            $0.onResponse { (response) in
                switch response.result {
                case .success(let response):
                    completion(.success(ControlPosResponseView.toControlPosResponse(response)))
                case .failure(let error):
                    completion(.failure(error))
                }
            }
            
            $0.onError { (error) in
                completion(.failure(error))
            }
        }
    }
    
    func updateItem(forSession sessionId: Int64, cartId: String, cartItemId: String, barcode: String, quantity: Double, completion: @escaping (Result<ControlPosResponse, Error>) -> Void) {
        guard let querySafeCartId = cartId.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed) else {
            completion(.failure(ApiError.unknown(causeBy: nil)))
            return
        }
        
        apiClient.request(decoder: JsonDecoder(decodable: ControlPosResponseView.self)) {
            $0.method = .patch
            $0.url = "/api/v2/controls/sessions/\(sessionId)/carts/\(querySafeCartId)/items"
            $0.bodyEncoder = JsonBodyEncoder(with: [
                "barcode" : barcode,
                "quantity": quantity,
                "receiptItemId" : cartItemId
            ])
        }.resume {
            $0.onResponse { (response) in
                switch response.result {
                case .success(let response):
                    completion(.success(ControlPosResponseView.toControlPosResponse(response)))
                case .failure(let error):
                    completion(.failure(error))
                }
            }
            
            $0.onError { (error) in
                completion(.failure(error))
            }
        }
    }
    
    func deleteItem(forSession sessionId: Int64, cartId: String, cartItemId: String, barcode: String, completion: @escaping (Result<ControlPosResponse, Error>) -> Void) {
        guard let querySafeCartId = cartId.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed), let querySafeCartItemId = cartItemId.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed) else {
            completion(.failure(ApiError.unknown(causeBy: nil)))
            return
        }
        
        apiClient.request(decoder: JsonDecoder(decodable: ControlPosResponseView.self)) {
            $0.method = .delete
            $0.url = "/api/v2/controls/sessions/\(sessionId)/carts/\(querySafeCartId)/items?barcode=\(barcode)&receiptitemid=\(querySafeCartItemId)"
        }.resume {
            $0.onResponse { (response) in
                switch response.result {
                case .success(let response):
                    completion(.success(ControlPosResponseView.toControlPosResponse(response)))
                case .failure(let error):
                    completion(.failure(error))
                }
            }
            
            $0.onError { (error) in
                completion(.failure(error))
            }
        }
    }
}
