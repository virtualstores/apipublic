
import Foundation
import Domain

fileprivate struct ItemDetailDto: Codable {
    let itemGroups: [ItemGroup]
    let items: [ItemDto]
}

public class ItemApiImpl: ItemApi {
	
	internal let apiClient: VirtualStoresApiClient
	
	internal init(apiClient: VirtualStoresApiClient) {
        self.apiClient = apiClient
    }
    
    
    public func getItemBy(id: String, completion: @escaping (Result<Item, Error>) -> Void) {
		apiClient.request(decoder: JsonDecoder.init(decodable: ItemDto.self)) {
			$0.method = .get
            $0.url = "/api/v2/items/\(id.trimmingCharacters(in: .whitespacesAndNewlines))"
		}.resume {
			$0.onResponse { (response) in
				completion(response.result.map { ItemDto.toItem(itemDto: $0) }.mapError { $0 as Error })
			}
			
			$0.onError { (error) in
				completion(.failure(error))
			}
		}
    }
    
    public func getRecommendedItems(for memberId: String, completion: @escaping (Result<[Item], Error>) -> Void) {
        apiClient.request(decoder: JsonDecoder.init(decodable: [ItemDto].self)) {
            $0.method = .get
            $0.url = "/api/v2/items/extension/recommended/\(memberId)"
        }.resume {
            $0.onResponse { (response) in
                switch response.result {
                case .success(let itemDtos):
                    let items = itemDtos.map { itemDto in
                        return ItemDto.toItem(itemDto: itemDto)
                    }
                    completion(.success(items))
                case .failure(let error):
                    completion(.failure(error))
                }
            }
            
            $0.onError { (error) in
                completion(.failure(error))
            }
        }
    }
    
    public func getItemGroup(limit: Int32, offset: Int32, completion: @escaping (Result<[ItemGroup], Error>) -> Void) {
        apiClient.request(decoder: JsonDecoder.init(decodable: [ItemGroup].self)) {
            $0.method = .get
            $0.url = "/api/v2/itemgroups"
            $0.bodyEncoder = URLBodyEncoder(parameters: [
                "limit": limit,
                "offset": offset
            ])
        }.resume {
            $0.onResponse { (response) in
                switch response.result {
                case .success(let groups):
                    completion(.success(groups))
                case .failure(let error):
                    completion(.failure(error))
                }
            }
        }
    }
    
    public func getItemGroupDetail(id: String, completion: @escaping (Result<ItemGroupDetail, Error>) -> Void) {
        apiClient.request(decoder: JsonDecoder.init(decodable: ItemDetailDto.self)) {
            $0.method = .get
            $0.url = "/api/v2/itemgroups/\(id.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "")"
        }.resume {
            $0.onResponse { (response) in
                switch response.result {
                case .success(let details):
                    completion(.success(ItemGroupDetail(subgroups: details.itemGroups, items: details.items.map { ItemDto.toItem(itemDto: $0) })))
                case .failure(let error):
                    completion(.failure(error))
                }
            }
        }
    }
    
    public func getItemDetail(id: String, completion: @escaping (Result<ItemDetail, Error>) -> Void) {
        apiClient.request(decoder: JsonDecoder.init(decodable: ItemDetail.self)) {
            $0.method = .get
            $0.url = "/api/v2/items/extension/\(id)"
        }.resume {
            $0.onResponse { (response) in
                completion(response.result.map { $0 }.mapError { $0 as Error })
            }
            
            $0.onError { (error) in
                completion(.failure(error))
            }
        }
    }
}

