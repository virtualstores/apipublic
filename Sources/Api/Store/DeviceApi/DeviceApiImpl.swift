//
//  DeviceApiImpl.swift
//  Api
//
//  Created by Jesper Lundqvist on 2020-04-15.
//  Copyright © 2020 Virtual Stores. All rights reserved.
//

import Domain

class DeviceApiImpl: DeviceApi {
    internal let apiClient: VirtualStoresApiClient
    
    internal init(apiClient: VirtualStoresApiClient) {
        self.apiClient = apiClient
    }
    
    func minimumVersion(completion: @escaping (Result<String, Error>) -> Void) {
        apiClient.request(decoder: JsonDecoder(decodable: MinimumVersion.self)) {
            $0.method = .get
            $0.url = "/api/v2/devices/information"
            
        }.resume {
            $0.onResponse { (response) in
                switch response.result {
                case .success(let minimumVersion):
                    completion(.success(minimumVersion.minimumIosVersion))
                case .failure(let error):
                    completion(.failure(error))
                }
            }
            
            $0.onError { (error) in
                completion(.failure(error))
            }
        }
    }
}
