//
//  CustomOfferDto.swift
//  Api
//
//  Created by Gustav Larson on 2020-01-21.
//

import Foundation

public struct CustomOfferDto: Codable {
    
    public enum OfferType: Int, Codable {
        case _0 = 0
        case _1 = 1
        case _2 = 2
    }
    
    public let title: String?
    public let offerPrice: Double?
    public let offerPriceUnitOfMeasure: String?
    public let originalPrice: Double?
    public let originalPriceUnitOfMeasure: String?
    public let sortPosition: Double?
    public let imageUrl: String?
    public let brand: String?
    public let decorator: String?
    public let redeemLimitLabel: String?
    public let volume: String?
    public let startDate: String?
    public let endDate: String?
    public let type: OfferType?
    public let ecoLabels: [EcoLabelDto]?
    public let products: [ItemDto]?
    public let conditionLabel: String?

    public init(title: String? = nil, offerPrice: Double? = nil, offerPriceUnitOfMeasure: String? = nil, originalPrice: Double? = nil, originalPriceUnitOfMeasure: String? = nil, sortPosition: Double? = nil, imageUrl: String? = nil, brand: String? = nil, decorator: String? = nil, redeemLimitLabel: String? = nil, volume: String? = nil, startDate: String? = nil, endDate: String? = nil, type: OfferType? = nil, ecoLabels: [EcoLabelDto]? = nil, products: [ItemDto]? = nil, conditionLabel: String? = nil) {
        self.title = title
        self.offerPrice = offerPrice
        self.offerPriceUnitOfMeasure = offerPriceUnitOfMeasure
        self.originalPrice = originalPrice
        self.originalPriceUnitOfMeasure = originalPriceUnitOfMeasure
        self.sortPosition = sortPosition
        self.imageUrl = imageUrl
        self.brand = brand
        self.decorator = decorator
        self.redeemLimitLabel = redeemLimitLabel
        self.volume = volume
        self.startDate = startDate
        self.endDate = endDate
        self.type = type
        self.ecoLabels = ecoLabels
        self.products = products
        self.conditionLabel = conditionLabel
    }
}
