
import Foundation
import Domain


public class OfferApiImpl: OfferApi {
    
    internal let apiClient: VirtualStoresApiClient
    
    internal init(apiClient: VirtualStoresApiClient) {
        self.apiClient = apiClient
    }
    
    public func getOffersBy(customerId: String, completion: @escaping (Result<[Offer], ApiError>) -> Void) {
        apiClient.request(decoder: JsonDecoder.init(decodable: [CustomOfferDto].self)) {
            $0.method = .get
            $0.url = "/api/v2/offers/customer"
            $0.bodyEncoder = URLBodyEncoder(parameters: [
                "memberid": customerId,
            ])
        }.resume {
            $0.onResponse { (response) in
                completion(response.result.map { CustomOfferDto.toOffers(offerDtos: $0) })
            }
            
            $0.onError { (error) in
                completion(.failure(error))
            }
        }
    }
}

