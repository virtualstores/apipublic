//
//  PaymentApiImpl.swift
//  Api
//
//  Created by Jesper Lundqvist on 2020-02-27.
//  Copyright © 2020 Virtual Stores. All rights reserved.
//

import Domain

public class PaymentApiImpl: PaymentApi {
    internal let apiClient: VirtualStoresApiClient
    
    internal init(apiClient: VirtualStoresApiClient) {
        self.apiClient = apiClient
    }
    
    public func getPaymentInformation(sessionId: Int64, cartId: String, completion: @escaping (Result<PaymentInformation, Error>) -> Void) {
        apiClient.request(decoder: JsonDecoder(decodable: PaymentInformation.self)) {
            $0.method = .get
            $0.url = "/api/v2/payments/sessions/\(sessionId)/carts/\(cartId.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed)!)"
        }.resume {
            $0.onResponse { response in
                switch response.result {
                case .success(let paymentInformation):
                    completion(.success(paymentInformation))
                case .failure(let error):
                    completion(.failure(error))
                }
            }
            
            $0.onError { error in
                completion(.failure(error))
            }
        }
    }
}
