//
//  CustomUserDto.swift
//  Api
//
//  Created by Gustav Larson on 2020-01-23.
//

import Foundation

public class CustomUserDto: Codable {
    public let id: Int?
    public let memberNumber: String?
    public let socialSecurity: String?
    public let externalCustomerid: String?
    public let lastLogin: String?
    public let controlLevel: Int?
    public let successCount: Int?
    public let controlFlag: Int?
    public let dateCreated: String?
    public let fullName: String?
    
    public init(id: Int? = nil, memberNumber: String? = nil, socialSecurity: String? = nil, externalCustomerid: String? = nil, lastLogin: String? = nil, controlLevel: Int? = nil, successCount: Int? = nil, controlFlag: Int? = nil, dateCreated: String? = nil, fullName: String? = nil) {
        self.id = id
        self.memberNumber = memberNumber
        self.socialSecurity = socialSecurity
        self.externalCustomerid = externalCustomerid
        self.lastLogin = lastLogin
        self.controlLevel = controlLevel
        self.successCount = successCount
        self.controlFlag = controlFlag
        self.dateCreated = dateCreated
        self.fullName = fullName
    }

}
