
import Foundation
import Domain


public class UserApiImpl: UserApi {
    internal let apiClient: VirtualStoresApiClient
    
    internal init(apiClient: VirtualStoresApiClient) {
        self.apiClient = apiClient
    }
    
    public func verifyUser(username: String, password: String, completion: @escaping (Result<LocalUser, Error>) -> Void) {
        apiClient.request(decoder: JsonDecoder.init(decodable: LocalUser.self)) {
            $0.method = .get
            $0.url = "/api/v2/customers/verify"
            $0.bodyEncoder = URLBodyEncoder(parameters: [
                "username": username,
                "password": password
            ])
        }.resume {
            $0.onResponse { (response) in
                completion(response.result.map { $0 as LocalUser }.mapError { $0 as Error })
            }
            
            $0.onError { (error) in
                completion(.failure(error))
            }
        }
    }
    
}

