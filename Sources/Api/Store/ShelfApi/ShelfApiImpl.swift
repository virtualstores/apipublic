//
//  ShelfApiImpl.swift
//  Alamofire
//
//  Created by Carl-Johan Dahlman on 2019-10-16.
//

import Foundation
import Domain


public class ShelfApiImpl: ShelfApi {
	internal let apiClient: VirtualStoresApiClient
	
	internal init(apiClient: VirtualStoresApiClient) {
		self.apiClient = apiClient
	}
	
    
    public func getShelfGroups(storeID: Int64, completion: @escaping (Result<[ShelfGroup], Error>) -> Void) {
		apiClient.request(decoder: JsonDecoder(decodable: [ShelfGroupDto].self)) {
			$0.method = .get
			$0.url = "api/v1/shelfgroups?storeId=\(storeID)"
		}.resume {
			$0.onResponse { (response) in
				completion(response.result.map { ShelfGroupDto.toShelfGroups($0) }.mapError { $0 as Error })
			}
			
			$0.onError { (error) in
				completion(.failure(error))
			}
		}
    }
    
    public func positionOnShelf(storeID: Int64, id: Int64, tierPosition: Int, barcode: String, overwrite: Bool, completion: @escaping (Error?) -> Void) {
        apiClient.request(decoder: JsonDecoder(decodable: String.self)) {
            $0.method = .post
            $0.url = "/api/v2/shelves/\(id)/items/position"
            $0.bodyEncoder = JsonBodyEncoder(with: [
                "tierPosition" : tierPosition,
                 "barcode" : barcode,
                 "overwrite" : overwrite
            ])
        }.resume {
            $0.onResponse { (response) in
                completion(nil)
            }
            
            $0.onError { (error) in
                completion(error)
            }
        }
    }
    
    public func removeFromShelf(storeID: Int64, barcode: String, completion: @escaping (Error?) -> Void) {
        apiClient.request(decoder: JsonDecoder(decodable: String.self)) {
            $0.method = .delete
            $0.url = "/api/v2/shelves/items/\(barcode)/position"
        }.resume {
            $0.onResponse { (response) in
                completion(nil)
            }
            
            $0.onError { (error) in
                completion(error)
            }
        }
    }
}
