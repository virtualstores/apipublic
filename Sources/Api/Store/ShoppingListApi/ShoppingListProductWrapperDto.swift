//
//  ShoppingListProductWrapperDto.swift
//  Api
//
//  Created by Gustav Larson on 2020-01-23.
//

import Foundation

public struct ShoppingListProductWrapperDto: Codable {
    
    public let id: Int?
    public let name: String?
    public let quantity: Double?
    public let price: Double?
    public let priceUnitOfMeasure: String?
    public let imageUrl: String?
    public let categoryName: String?
    public let brand: String?
    public let volume: String?
    public let item: ItemDto?
    
    public init(id: Int? = nil, name: String? = nil, quantity: Double? = nil, price: Double? = nil, priceUnitOfMeasure: String? = nil, imageUrl: String? = nil, categoryName: String? = nil, brand: String? = nil, volume: String? = nil, item: ItemDto? = nil) {
        self.id = id
        self.name = name
        self.quantity = quantity
        self.price = price
        self.priceUnitOfMeasure = priceUnitOfMeasure
        self.imageUrl = imageUrl
        self.categoryName = categoryName
        self.brand = brand
        self.volume = volume
        self.item = item
    }
    
}
