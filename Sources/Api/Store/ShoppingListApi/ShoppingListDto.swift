//
//  ShoppingListDto.swift
//  Api
//
//  Created by Gustav Larson on 2020-01-23.
//

import Foundation

public struct ShoppingListDto: Codable {
    public let id: String?
    public let name: String?
    public let description: String?
    public let products: [ShoppingListProductWrapperDto]?
    
    public init(id: String? = nil, name: String? = nil, description: String? = nil, products: [ShoppingListProductWrapperDto]? = nil) {
        self.id = id
        self.name = name
        self.description = description
        self.products = products
    }
}
