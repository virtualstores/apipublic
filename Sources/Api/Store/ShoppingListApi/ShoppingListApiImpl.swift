//
//  ShoppingListApiImpl.swift
//  Api
//
//  Created by Gustav Larson on 2020-01-23.
//

import Foundation
import Domain

public class ShoppingListApiImpl: ShoppingListApi {
    internal let apiClient: VirtualStoresApiClient

    internal init(apiClient: VirtualStoresApiClient) {
        self.apiClient = apiClient
    }


    public func getShoppingListsBy(customerId: String, completion: @escaping (Result<[ShoppingList], Error>) -> Void) {//TODO
        apiClient.request(decoder: JsonDecoder.init(decodable: [ShoppingListDto].self)) {
            $0.method = .get
            $0.url = "/api/v2/shoppinglist/customer"
            $0.bodyEncoder = URLBodyEncoder(parameters: [
                "memberid": customerId,

            ])
        }.resume {
            $0.onResponse { (response) in
                completion(response.result.map { ShoppingListDto.toShoppingLists(dtos: $0) }.mapError { $0 as Error })
            }
            
            $0.onError { (error) in
                completion(.failure(error))
            }
        }
    }
}
