//
//  StoreApiCollectionImpl.swift
//  Alamofire
//
//  Created by Carl-Johan Dahlman on 2019-10-14.
//

import Foundation
import Domain


class StoreApiCollectionImpl: StoreApiCollection {
    
	internal let apiClient: VirtualStoresApiClient
	
    init(connection: ServerConnection) {
        apiClient = VirtualStoresApiClient(connection: connection)
    }
    
    
    
    var itemApi: ItemApi {
        get {
            return ItemApiImpl(apiClient: apiClient)
        }
    }
    
    var searchApi: SearchApi {
        get {
            return SearchApiImpl(apiClient: apiClient)
        }
    }
    
    var shelfApi: ShelfApi {
        get {
            return  ShelfApiImpl(apiClient: apiClient)
        }
    }
    
    var  mapApi: MapApi {
        get {
            return MapApiImpl(apiClient: apiClient)
        }
    }
    var shopApi: ShopApi {
        get {
            return ShopApiImpl(apiClient: apiClient)
        }
    }
    
    var klarnaPaymentsApi: KlarnaPaymentsApi {
        get {
            return KlarnaPaymentsApiImpl(apiClient: apiClient)
        }
    }
	
	var payExCardPaymentsApi: PayExCardPaymentsApi {
		PayExCardPaymentsApiImpl(apiClient: apiClient)
	}
	
	var payExSwishPaymentsApi: PayExSwishPaymentsApi {
		PayExSwishPaymentsApiImpl(apiClient: apiClient)
	}
    
    var offerApi: OfferApi {
        get {
            return OfferApiImpl(apiClient: apiClient)
        }
    }
    
    var shoppingListApi: ShoppingListApi {
        get {
            return ShoppingListApiImpl(apiClient: apiClient)
        }
    }
    
    var userApi: UserApi {
        get {
            return UserApiImpl(apiClient: apiClient)
        }
    }
    
    var recipeApi: RecipeApi {
        get {
            return RecipeApiImpl(apiClient: apiClient)
        }
    }
    
    var statisticsApi: StatisticsApi {
        get {
            return StatisticsApiImpl(apiClient: apiClient)
        }
    }
    
    var paymentApi: PaymentApi {
        get {
            return PaymentApiImpl(apiClient: apiClient)
        }
    }
    
    var deviceApi: DeviceApi {
        get {
            return DeviceApiImpl(apiClient: apiClient)
        }
    }
    
    var healthApi: HealthApi {
        get {
            return HealthApiImpl(apiClient: apiClient)
        }
    }
    
    var informationManagerApi: InformationManagerApi {
        get {
            return InformationManagerApiImpl(apiClient: apiClient)
        }
    }
    
    var controlApi: ControlApi {
        get {
            return ControlApiImpl(apiClient: apiClient)
        }
    }
}
