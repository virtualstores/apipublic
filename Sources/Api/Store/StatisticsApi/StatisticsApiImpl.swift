//
//  StatisticsApi.swift
//  Api
//
//  Created by Jesper Lundqvist on 2020-02-25.
//

import Foundation
import Domain

public class StatisticsApiImpl: StatisticsApi {
    
    internal let apiClient: VirtualStoresApiClient
    
    internal init(apiClient: VirtualStoresApiClient) {
        self.apiClient = apiClient
    }
    
    public func uploadVisit(_ visit: Visit, completion: @escaping (Error?) -> Void) {
        apiClient.request(decoder: JsonDecoder(type: Any.self)) {
            $0.method = .post
            $0.url = "/api/v2/statistics/visits"
            $0.bodyEncoder = JsonBodyEncoder(with: [
                "sessionId": visit.sessionId,
                "customerHeight": visit.customerHeight,
                "start": visit.start,
                "stop": visit.stop,
                "positionType": visit.positionType,
                "operatingSystem": visit.operatingSystem,
                "osVersion": visit.osVersion,
                "appVersion": visit.appVersion,
                "deviceModel": visit.deviceModel,
                "positions": visit.positions.map {[
                    "_id": $0._id,
                    "visitId": $0.visitId,
                    "x": $0.x,
                    "y": $0.y,
                    "health": $0.health,
                    "direction": $0.direction,
                    "timestamp": $0.timestamp
                ]},
                "scanEvents": []
            ])
        }.resume {
            $0.onResponse { _ in
                completion(nil)
            }
            
            $0.onError { error in
                completion(error)
            }
        }
    }
}
