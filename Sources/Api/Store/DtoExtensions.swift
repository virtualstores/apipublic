
import Foundation
import Domain


public extension ItemDto {
    static func toItem(itemDto: ItemDto) -> Item {
        return Item(
            id: itemDto._id ?? "",
            name: itemDto.name ?? "",
            prettyName: itemDto.prettyName ?? "",
            description: itemDto._description ?? "",
            manufacturerName: itemDto.manufacturerName ?? "",
            imageUrl: itemDto.imageUrl,
            alternateIds: itemDto.alternateIds?.map(ItemAlternateIdDto.toAlternateId),
            ecoLabels: itemDto.ecoLabels?.map(EcoLabelDto.toEcoLabel),
            shelfTierIds: itemDto.shelfTierItemRelations?.map(ShelfTierItemRelationDto.toShelfTierId),
            offerShelfTierIds: itemDto.offerItemPositions?.map(OfferItemShelfDto.toShelfTierId),
            offerItemId: itemDto.offerItemId,
            price: itemDto.price
        )
    }
}

public extension CustomOfferDto {
    static func toOffers(offerDtos: [CustomOfferDto]) -> [Offer] {
        return offerDtos.map(CustomOfferDto.toOffer)
    }
    
    static func toOffer(offerDto: CustomOfferDto) -> Offer {
        
        return Offer(
            title: offerDto.title ?? "",
            offerPrice: offerDto.offerPrice ?? 0,
            offerPriceUnitOfMeasure: offerDto.offerPriceUnitOfMeasure ?? "",
            originalPrice: offerDto.originalPrice ?? 0,
            originalPriceUnitOfMeasure: offerDto.originalPriceUnitOfMeasure ?? "",
            sortPosition: offerDto.sortPosition ?? 0,
            imageUrl: offerDto.imageUrl ?? "",
            brand: offerDto.brand ?? "",
            decorator: offerDto.decorator,
            redeemLimitLabel: offerDto.redeemLimitLabel,
            volume: offerDto.volume,
            startDate: offerDto.startDate ?? "",
            endDate: offerDto.endDate ?? "",
            offerType: Offer.OfferType(rawValue: offerDto.type!.rawValue) ?? Offer.OfferType.general,
            ecoLabels: offerDto.ecoLabels?.map(EcoLabelDto.toEcoLabel) ?? [],
            products: offerDto.products?.map(ItemDto.toItem) ?? [],
            conditionLabel: offerDto.conditionLabel
        )
    }
}

public extension ItemAlternateIdDto {
    static func toAlternateId(_ alternateIdDto: ItemAlternateIdDto) -> AlternateId {
        return AlternateId(
            id: alternateIdDto._id ?? "",
            itemId: alternateIdDto.itemId ?? "",
            type: AlternateId.IdType(rawValue: alternateIdDto.type!.rawValue) ?? AlternateId.IdType.unknown
        )
    }
}
public extension EcoLabelDto {
    static func toEcoLabel(_ ecoLabelDto: EcoLabelDto) -> EcoLabel {
        return EcoLabel(
            id: 0,
            name: ecoLabelDto.name ?? "",
            prettyName: ecoLabelDto.prettyName ?? "",
            imageUrl: ecoLabelDto.imageUrl ?? ""
        )
    }
}

public extension ShelfTierItemRelationDto {
    static func toShelfTierId(_ shelfTierItemRelationDto:ShelfTierItemRelationDto) -> Int64 {
        return shelfTierItemRelationDto.shelfTierId ?? 0
    }
}

public extension ItemGroupDto {
    static func toItemGroup(_ itemGroupDto: ItemGroupDto) -> ItemGroup {
        return ItemGroup(
            id: itemGroupDto._id ?? "",
            name: itemGroupDto.name ?? "",
            prettyName: itemGroupDto.prettyName ?? "",
            imageUrl: itemGroupDto.imageUrl,
            itemPosition: itemGroupDto.itemPosition.flatMap({ (dto: ItemPositionDto) -> ItemPosition in
                ItemPosition(x: Float(dto.x ?? 0.0), y: Float(dto.y ?? 0.0), offsetX: Float(dto.offsetX ?? 0.0), offsetY: Float(dto.offsetY ?? 0.0))
            }),
            parentItemGroupId: itemGroupDto.parentItemGroupId)
    }
}

public extension SearchResultResponse {
    static func toSearchResult(_ searchResultResponse: SearchResultResponse)-> SearchResult {
        return SearchResult(
            itemGroups: searchResultResponse.itemGroups?.map(ItemGroupDto.toItemGroup) ?? [],
            items: searchResultResponse.items?.map(ItemDto.toItem) ?? []
        )
    }
}

public extension ShelfGroupDto {
    static func toShelfGroups(_ dtos: [ShelfGroupDto]) -> [ShelfGroup] {
        return dtos.map(ShelfGroupDto.toShelfGroup)
    }
    
    static func toShelfGroup(_ dto: ShelfGroupDto) -> ShelfGroup {
        return ShelfGroup(
            id: dto._id ?? 0,
            name: dto.name ?? "",
            itemPosition: ItemPosition(
                x: Float(dto.itemPositionX ?? 0),
                y: Float(dto.itemPositionY ?? 0),
                offsetX: 0,
                offsetY: 0),
            shelves: dto.shelves?.map(ShelfDto.toShelf) ?? []
        )
    }
}

public extension ShelfDto {
    static func toShelf(_ dto: ShelfDto) -> Shelf {
        return Shelf(
            id: dto._id ?? 0,
            name: dto.name ?? "",
            itemPosition: ItemPosition(
                x: Float(dto.itemPositionX ?? 0),
                y: Float(dto.itemPositionY ?? 0),
                offsetX: Float(dto.itemPositionOffsetX ?? 0),
                offsetY: Float(dto.itemPositionOffsetY ?? 0)
            ),
            shelfGroupPosition: Int(dto.shelfGroupId ?? 0),
            shelfTiers: dto.shelfTiers?.map(ShelfTierDto.toShelfTier) ?? [],
            shape: dto.points!.map({ (point) -> CGPoint in
                CGPoint(x: point.x!, y: point.y!)
            })
        )
    }
}

public extension RegularShelfGroupDto {
    
    static func toShelfGroups(_ dtos: [RegularShelfGroupDto]) -> [ShelfGroup] {
        return dtos.map(RegularShelfGroupDto.toShelfGroup)
    }
    
    static func toShelfGroup(_ dto: RegularShelfGroupDto) -> ShelfGroup {
        return ShelfGroup(
            id: dto._id ?? 0,
            name: dto.name ?? "",
            itemPosition: ItemPosition(
                x: Float(dto.itemPositionX ?? 0),
                y: Float(dto.itemPositionY ?? 0),
                offsetX: 0,
                offsetY: 0),
            shelves: dto.shelves?.map(RegularShelfDto.toShelf) ?? []
        )
    }
}

public extension RegularShelfDto {
    static func toShelf(_ dto: RegularShelfDto) -> Shelf {
        return Shelf(
            id: dto._id ?? 0,
            name: dto.name ?? "",
            itemPosition: ItemPosition(
                x: Float(dto.itemPositionX ?? 0),
                y: Float(dto.itemPositionY ?? 0),
                offsetX: Float(dto.itemPositionOffsetX ?? 0),
                offsetY: Float(dto.itemPositionOffsetY ?? 0)
            ),
            shelfGroupPosition: dto.shelfGroupPosition ?? 0,
            shelfTiers: dto.shelfTiers?.map(ShelfTierDto.toShelfTier) ?? [],
            shape: [
                CGPoint(x: dto.shape?.topLeft?.x ?? 0, y: dto.shape?.topLeft?.y ?? 0),
                CGPoint(x: dto.shape?.topRight?.x ?? 0, y: dto.shape?.topRight?.y ?? 0),
                CGPoint(x: dto.shape?.bottomRight?.x ?? 0, y: dto.shape?.bottomRight?.y ?? 0),
                CGPoint(x: dto.shape?.bottomLeft?.x ?? 0, y: dto.shape?.bottomLeft?.y ?? 0)
            ]
        )
    }
}

public extension ShelfTierDto {
    static func toShelfTier(_ dto: ShelfTierDto) -> ShelfTier {
        return ShelfTier(
            id: dto._id ?? 0,
            shelfId: dto.shelfId ?? 0,
            shelfPosition: dto.shelfPosition ?? 0
        )
    }
}

public extension RtlsOptionDto {
    static func toRtlsOption(_ dto: RtlsOptionDto) -> RtlsOption {
        return RtlsOption(
            storeId: dto.storeId ?? 0,
            width: dto.width ?? 0,
            height: dto.height ?? 0,
            startOffsetX: dto.startOffsetX ?? 0,
            startOffsetY: dto.startOffsetY ?? 0,
            waitTime: dto.waitTime ?? 0,
            panId: dto.panId ?? "")
    }
}

public extension MapDto {
    static func toMap(_ dto: MapDto) -> Map {
        return Map(
            id: dto._id ?? 0,
            mapURL: dto.mapURL ?? "",
            storeId: dto.storeId ?? 0,
            railScale: dto.railScale ?? 0,
            pixelOffsetX: dto.pixelOffsetX ?? 0,
            pixelOffsetY: dto.pixelOffsetY ?? 0,
            pixelWidth: dto.pixelWidth ?? 0,
            pixelHeight: dto.pixelHeight ?? 0)
    }
}

public extension PosResponseView {
    static func toPosResponse(_ dto: PosResponseView) -> PosResponse {
        return PosResponse(
            customerName: dto.customerName,
            referenceCode: dto.referenceCode,
            gateCode: dto.gateCode,
            hasControl: dto.hasControl,
            carts: dto.carts?.map(CartView.toCart) ?? []) 
    }
}

public extension ControlPosResponseView {
    static func toControlPosResponse(_ dto: ControlPosResponseView) -> ControlPosResponse {
        return ControlPosResponse(
            controlFlag: dto.controlFlag,
            posResponse: PosResponseView.toPosResponse(dto.posResponse))
    }
}

public extension SessionFromReferenceDto {
    static func toSession(_ dto: SessionFromReferenceDto) -> Session? {
        guard let id = dto.id, let stateId = dto.state, let state = Session.State(rawValue: stateId) else {
            return nil
        }
        
        return Session(id: id, state: state)
    }
}

public extension CartView {
    static func toCart(_ dto: CartView) -> Cart {
        return Cart(
            id: dto._id ?? "",
            state: Cart.State.init(rawValue: dto.state!.rawValue)!,
            items: dto.items?.map(CartItemView.toCartItem) ?? [],
            totals: CartTotalsView.toCartTotals(dto.totals!),
            receiptNumber: dto.receiptNumber ?? "")
    }
}

public extension CartTotalsView {
    static func toCartTotals(_ dto: CartTotalsView)-> CartTotals{
        return CartTotals(
            discountAmount: dto.discountAmount ?? 0,
            deliveredNowCartItemsTotalAmount: dto.deliveredNowCartItemsTotalAmount ?? 0,
            saletotalAmount: dto.saletotalAmount ?? 0,
            saletotalExclusiveVATAmount: dto.saletotalExclusiveVATAmount ?? 0,
            vatAmount: dto.vatAmount ?? 0)
    }
}

public extension CartItemView {
    static func toCartItem(_ dto: CartItemView) -> CartItem {
        return CartItem(
            id: dto._id!,
            parentId: dto.parentId ?? 0,
            totalPrice: dto.totalPrice!,
            article: CartItemArticleView.toCartIemAricle(dto.article),
            quantity: CartItemQuantityView.toCartItemQuantity(dto.quantity),
            vat: CartItemVatView.toCartItemVat(dto.vat),
            discountContributions: dto.discountContributions?.map(CartItemDiscountContributionView.toCartItemDiscountContribution),
            discountDetails: dto.discountDetails?.map(CartItemDiscountDetailView.toCartItemDiscountDetail),
            depositInfo: CartItemDepositInfoView.toCartItemDepositInfo(dto.depositInfo))
    }
}

public extension CartItemArticleView {
    static func toCartIemAricle(_ dto: CartItemArticleView?) -> CartItemArticle? {
        if  dto == nil {
            return nil
        }
        
        return CartItemArticle(
            colorText: dto?.colorText,
            colorCode: dto?.colorCode,
            fullText: dto?.fullText,
            productInformation: dto?.productInformation,
            unitPrice: dto?.unitPrice,
            ageRequirement: dto?.ageRequirement,
            barcodes: dto?.barcodes)
    }
}

public extension CartItemQuantityView {
    static func toCartItemQuantity(_ dto: CartItemQuantityView?) -> CartItemQuantity? {
        if  dto == nil {
            return nil
        }
        
        let unitOfMeasure = dto?.unitOfMeasure.flatMap { UnitOfMeasure(rawValue: $0) }
        
        return CartItemQuantity(
            deliveredNow: dto?.deliveredNow,
            quantity: dto?.quantity,
            unitOfMeasure: unitOfMeasure,
            updateAllowed: dto?.updateAllowed)
    }
}

public extension CartItemVatView {
    static func toCartItemVat(_ dto: CartItemVatView?) -> CartItemVat? {
        if  dto == nil {
            return nil
        }
        return CartItemVat(
            vatPercent: dto?.vatPercent,
            vatTotal: dto?.vatTotal)
    }
}

public extension CartItemDiscountContributionView {
    static func toCartItemDiscountContribution(_ dto: CartItemDiscountContributionView) -> CartItemDiscountContribution {
        
        return CartItemDiscountContribution(
            id: dto._id,
            description: dto._description,
            quantity: dto.quantity)
    }
}

public extension CartItemDiscountDetailView {
    static func toCartItemDiscountDetail(_ dto: CartItemDiscountDetailView) -> CartItemDiscountDetail {
        return CartItemDiscountDetail(
            description: dto._description,
            discountAmountPerQuantity: dto.discountAmountPerQuantity,
            discountEffectType: dto.discountEffectType,
            discountEffectTypeAmount: dto.discountEffectTypeAmount,
            discountType: dto.discountType,
            quantity: dto.quantity,
            totalDiscountAmount: dto.totalDiscountAmount)
    }
}

public extension CartItemDepositInfoView {
    static func toCartItemDepositInfo(_ dto: CartItemDepositInfoView?) -> CartItemDepositInfo? {
        guard let dto = dto else {
            return nil
        }
        return CartItemDepositInfo(depositUnitPrice: dto.depositUnitPrice)
    }
}

public extension ShoppingSessionDto {
    static func toSession(_ dto: ShoppingSessionDto?) -> Session? {
        if dto == nil {
            return nil
        }
        
        return Session(id: dto!._id!, state: Session.State(rawValue: dto!.state!.rawValue)!)
    }
}

public extension CustomSessionDto {
    static func toSession(_ dto: CustomSessionDto?) -> Session? {
        if  dto == nil {
            return nil
        }
        
        return Session(id: dto!._id!, state: Session.State(rawValue: dto!.state!.rawValue)!)
    }
}

public extension PayExOperationDto {
    static func toPayExOperation(_ dto: PayExOperationDto) -> PayExOperation {
        return PayExOperation(href: dto.href ?? "", rel: dto.rel ?? "", method: dto.method ?? "", contentType: dto.contentType ?? "")
    }
}

public extension KlarnaPaymentSessionDto {
    static func toKlarnaPaymentSession(_ dto: KlarnaPaymentSessionDto) -> KlarnaPaymentSession {
        return KlarnaPaymentSession(
            sessionId: dto.sessionId ?? "",
            clientToken: dto.clientToken ?? "",
            paymentMethodCategories: dto.paymentMethodCategories?.map{
                KlarnaPaymentSession.PaymentMethodCategory(
                    identifier: $0.identifier ?? "",
                    name: $0.name ?? "",
                    assetUrls: KlarnaPaymentSession.AssetUrls(
                        descriptive: $0.assetUrls?.descriptive ?? "",
                        standard: $0.assetUrls?.descriptive ?? "")
                )
            } ?? []
        )
    }
}

public extension ShoppingListDto {
    static func toShoppingLists(dtos: [ShoppingListDto]) -> [ShoppingList] {
        return dtos.map(ShoppingListDto.toShoppingList)
    }
    
    static func toShoppingList(dto: ShoppingListDto) -> ShoppingList {
        return ShoppingList(
            id: dto.id ?? "",
            name: dto.name ?? "",
            description: dto.description ?? "",
            products: dto.products?.map(ShoppingListProductWrapperDto.toShoppingListItem) ?? []
        )
    }
}

public extension ShoppingListProductWrapperDto {
    static func toShoppingListItem(dto: ShoppingListProductWrapperDto) -> ShoppingListItem {
        let item: Item?
        if (dto.item != nil) {
            item = ItemDto.toItem(itemDto: dto.item!)
        } else {
            item = nil
        }
        
        return ShoppingListItem(
            id: "\(dto.id)" ?? "",
            name: dto.name ?? "",
            quantity: dto.quantity ?? 1,
            price: dto.price,
            priceUnitOfMeasure: dto.priceUnitOfMeasure,
            imageUrl: dto.imageUrl,
            categoryName: dto.categoryName,
            brand: dto.brand,
            volume: dto.volume,
            item: item
        )
    }
}

public extension CustomUserDto {
    static func toUser(dto: CustomUserDto) -> User {
        return User(
            name: dto.fullName,
            socialSecurity: dto.socialSecurity ?? "",
            memberNumber: dto.memberNumber ?? ""
        )
    }
}

public extension CustomRecipeDto {
    static func toRecipes(dtos: [CustomRecipeDto]) -> [Recipe] {
        return dtos.map(CustomRecipeDto.toRecipe)
    }
    
    static func toRecipe(dto: CustomRecipeDto) -> Recipe {
        return Recipe(
            id: dto.id ?? 0,
            recipeId: dto.recipeId ?? "",
            name: dto.name ?? "",
            imageUrl: dto.imageUrl ?? "",
            recipeUrl: dto.recipeUrl ?? "",
            cookingTimeInMinutes: Int(dto.cookingTimeInMinutes ?? 0),
            numberOfMeals: dto.numberOfMeals?.map{Int($0)} ?? [],
            categories: dto.categories ?? [],
            allergies: dto.allergies ?? [],
            cookingInstruction: dto.cookingInstruction ?? [],
            ingredientSections: dto.ingredientSections?.map(CustomIngredientSectionDto.toIngredientSection) ?? []
        )
    }
}

public extension CustomIngredientSectionDto {
    static func toIngredientSection(dto: CustomIngredientSectionDto ) -> IngredientSection {
        return IngredientSection(
            sectionName: dto.sectionName,
            ingredients: dto.ingredients?.map(CustomIngredientDto.toIngredient) ?? []
        )
    }
}

public extension CustomIngredientDto {
    static func toIngredient(dto: CustomIngredientDto) -> Ingredient {
        return Ingredient(
            name: dto.name ?? "",
            unit: dto.unit ?? "",
            optional: dto.optional ?? false,
            quantity: dto.quantity ?? [],
            item: CustomRecipeItemDto.toRecipeItem(dto: dto.item)
        )
    }
}

public extension CustomRecipeItemDto {
    static func toRecipeItem(dto: CustomRecipeItemDto?) -> RecipeItem? {
        guard let dto = dto else {
            return nil
        }
        
        return RecipeItem(
            barcode: dto.barcode ?? "",
            code: dto.code ?? "",
            imageUrl: dto.imageUrl,
            name: dto.name ?? "",
            unit: dto.unit ?? "",
            shelfTierIds: dto.shelfTierIds ?? [],
            quantity: dto.quantity ?? []
        )
    }
}
