#!/bin/bash

echo "Closing project..."


osascript \
    -e 'tell application "Xcode"' \
        -e 'close front window' \
    -e 'end tell'

sleep 2

echo "Updating project..."

swift package update
swift package generate-xcodeproj

echo "Restarting project"

open Api.xcodeproj
