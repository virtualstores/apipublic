Pod::Spec.new do |spec|
  spec.name         = "Api"
  spec.version      = "1.6.3"
  spec.summary      = "The Api implementation"
  spec.description  = "The api implementation for the device and central server"
  spec.homepage     = "http://virtualstores.se"
  spec.license      = "MIT"
  spec.author             = { "cj" => "carljohan.dahlman@virtualstores.se" }
  spec.platform     = :ios
  spec.ios.deployment_target = "11.0"
  spec.swift_version = '5.0'

  spec.source       = { :git => "http://bitbucket.org/virtualstores/ApiPublic.git", :tag => "#{spec.version}" }

  spec.source_files  = "Sources", "Sources/**/*.{h,m,swift}"
  spec.exclude_files = "Classes/Exclude"

  spec.dependency "Domain", "~> 1.6.3"
end
