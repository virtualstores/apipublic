//
//  NavigationSpaceApiTest.swift
//  ApiTest
//
//  Created by Jesper Lundqvist on 2020-06-22.
//  Copyright © 2020 Virtual Stores. All rights reserved.
//

import XCTest
import Domain
import Api

class NavigationSpaceApiTest: XCTestCase {
    
    var api: Api!
    
    override func setUpWithError() throws {
        api = ApiImpl(centralConnection: TestEnvironment.centralConnection, clientId: TestEnvironment.clientId)
    }

    override func tearDownWithError() throws {
    }
    
    func testNavigationSpace() throws {
        let expectation = XCTestExpectation()
        
        api.central.centralApi.getNavigationSpace(for: 1) { result in
            switch result {
            case .success(let spaces):
                print(spaces)
            case .failure(let error):
                XCTFail(error.localizedDescription)
            }
            
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 10.0)
    }
}
