//
//  UserApiTest.swift
//  ApiTest
//
//  Created by Jesper Lundqvist on 2020-06-02.
//  Copyright © 2020 Virtual Stores. All rights reserved.
//

import XCTest
import Domain
import Api

class UserApiTest: XCTestCase {
    
    var api: Api!
    
    override func setUpWithError() throws {
        api = ApiImpl(centralConnection: TestEnvironment.centralConnection, clientId: TestEnvironment.clientId)
        api.initStoreApi(storeConnection: TestEnvironment.retailStoreConnection)
    }

    override func tearDownWithError() throws {
    }
    
    func testUserVerify() throws {
        let expectation = XCTestExpectation()
        
        api.store?.userApi.verifyUser(username: TestEnvironment.retailUser.socialSecurity!, password: "123") { result in
            switch result {
            case .success(let user):
                XCTAssert(user.fullName == TestEnvironment.retailUser.name)
            case .failure(let error):
                XCTFail(error.localizedDescription)
            }
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 10.0)
    }
    
}
