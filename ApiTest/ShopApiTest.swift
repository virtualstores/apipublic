//
//  ShopApiTest.swift
//  ApiTest
//
//  Created by Jesper Lundqvist on 2020-06-02.
//  Copyright © 2020 Virtual Stores. All rights reserved.
//

import XCTest
import Domain
import Api

class ShopApiTest: XCTestCase {
    
    var api: Api!
    var sessionId: Int64!
    var cartId: String!
    
    override func setUpWithError() throws {
        api = ApiImpl(centralConnection: TestEnvironment.centralConnection, clientId: TestEnvironment.clientId)
        (api as? ApiImpl)?.loggingLevel = .medium
        api.initStoreApi(storeConnection: TestEnvironment.retailStoreConnection)
    }

    override func tearDownWithError() throws {
    }
    
    func testMobileSession() throws {
        let expectation = XCTestExpectation()
        api.initStoreApi(storeConnection: .init(id: 0, storeId: 0, serverUrl: "https://gunnis-hp-local.ih.vs-office.se", mqttUrl: "", apiKey: "kanelbulle"))
        api.store?.shopApi.createMobileSession(identificationNumber: TestEnvironment.retailUser.socialSecurity!, createCart: true, receiptLabel: nil) { result in
            switch result {
            case .success(let session):
                self.api.store?.shopApi.finalize(sessionId: session.id, devicePayment: false) { result in
                    switch result {
                    case .success:
                        self.api.store?.shopApi.getSession(sessionId: session.id) { result in
                            switch result {
                            case .success(let session):
                                XCTAssert(session.state == .finished)
                            case .failure(let error):
                                XCTFail(error.localizedDescription)
                            }
                            expectation.fulfill()
                        }
                    case .failure(let error):
                        XCTFail(error.localizedDescription)
                        expectation.fulfill()
                    }
                }
            case .failure(let error):
                XCTFail(error.localizedDescription)
                expectation.fulfill()
            }
        }
        wait(for: [expectation], timeout: 10.0)
    }
    
    func testSession() throws {
        let expectation = XCTestExpectation()
        api.store?.shopApi.create(identificationNumber: TestEnvironment.retailUser.socialSecurity!, createCart: true) { result in
            switch result {
            case .success(let session):
                self.api.store?.shopApi.activate(sessionId: session.id) { error in
                    if let error = error {
                        XCTFail(error.localizedDescription)
                    }
                    else {
                        self.api.store?.shopApi.finalize(sessionId: session.id, devicePayment: false) { result in
                            switch result {
                            case .success:
                                self.api.store?.shopApi.getSession(sessionId: session.id) { result in
                                    switch result {
                                    case .success(let session):
                                        XCTAssert(session.state == .finished)
                                    case .failure(let error):
                                        XCTFail(error.localizedDescription)
                                    }
                                     expectation.fulfill()
                                }
                            case .failure(let error):
                                XCTFail(error.localizedDescription)
                            }
                        }
                    }
                }
            case .failure(let error):
                XCTFail(error.localizedDescription)
            }
        }
        wait(for: [expectation], timeout: 10.0)
    }
    
    func testSessionAddCart() throws {
        let expectation = XCTestExpectation()
        api.store?.shopApi.create(identificationNumber: TestEnvironment.retailUser.socialSecurity!, createCart: false) { result in
            switch result {
            case .success(let session):
                self.api.store?.shopApi.activate(sessionId: session.id) { error in
                    if let error = error {
                        XCTFail(error.localizedDescription)
                    }
                    else {
                        self.api.store?.shopApi.addCart(sessionId: session.id, cartId: "test") { result in
                            switch result {
                            case .success:
                                self.api.store?.shopApi.finalize(sessionId: session.id, devicePayment: false) { result in
                                    switch result {
                                    case .success:
                                        self.api.store?.shopApi.getSession(sessionId: session.id) { result in
                                            switch result {
                                            case .success(let session):
                                                XCTAssert(session.state == .finished)
                                            case .failure(let error):
                                                XCTFail(error.localizedDescription)
                                            }
                                             expectation.fulfill()
                                        }
                                    case .failure(let error):
                                        XCTFail(error.localizedDescription)
                                    }
                                }
                            case .failure(let error):
                                XCTFail(error.localizedDescription)
                            }
                        }
                    }
                }
            case .failure(let error):
                XCTFail(error.localizedDescription)
            }
        }
        wait(for: [expectation], timeout: 10.0)
    }
    
    func testRecoverSession() throws {
        api.initStoreApi(storeConnection: TestEnvironment.constructionStoreConnection)
        
        let expectation = XCTestExpectation()
        api.store?.shopApi.create(identificationNumber: TestEnvironment.constructionUser.memberNumber, createCart: true) { result in
            switch result {
            case .success(let session):
                self.api.store?.shopApi.activate(sessionId: session.id) { error in
                    if let error = error {
                        XCTFail(error.localizedDescription)
                    }
                    else {
                        self.api.store?.shopApi.getSession(referenceCode: TestEnvironment.constructionUser.memberNumber) { result in
                            switch result {
                            case .success(let fetchedSession):
                                self.api.store?.shopApi.finalize(sessionId: fetchedSession.id, devicePayment: false) { result in
                                    switch result {
                                    case .success:
                                        self.api.store?.shopApi.getSession(sessionId: fetchedSession.id) { result in
                                            switch result {
                                            case .success(let session):
                                                XCTAssert(session.state == .finished)
                                            case .failure(let error):
                                                XCTFail(error.localizedDescription)
                                            }
                                             expectation.fulfill()
                                        }
                                    case .failure(let error):
                                        XCTFail(error.localizedDescription)
                                    }
                                }
                            case .failure(let error):
                                XCTFail(error.localizedDescription)
                            }
                        }
                    }
                }
            case .failure(let error):
                XCTFail(error.localizedDescription)
            }
        }
        wait(for: [expectation], timeout: 10.0)
    }
}
