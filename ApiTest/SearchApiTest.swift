//
//  SearchApiTest.swift
//  ApiTest
//
//  Created by Jesper Lundqvist on 2020-06-02.
//  Copyright © 2020 Virtual Stores. All rights reserved.
//

import XCTest
import Domain
import Api

class SearchApiTest: XCTestCase {
    
    var api: Api!
    
    override func setUpWithError() throws {
        api = ApiImpl(centralConnection: TestEnvironment.centralConnection, clientId: TestEnvironment.clientId)
        api.initStoreApi(storeConnection: TestEnvironment.retailStoreConnection)
    }

    override func tearDownWithError() throws {
    }
    
    func testSearch() throws {
        let expectation = XCTestExpectation()
        
        api.store?.searchApi.findBy(withExtension: true, searchvalue: "mjölk 3", onlyWithPosition: true, itemGroupId: nil, limit: 1) { result in
            switch result {
            case .success(let result):
                XCTAssert(result.items.first?.name == "Eko standardmjölk 3%")
            case .failure(let error):
                XCTFail(error.localizedDescription)
            }
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 10.0)
    }
    
}
