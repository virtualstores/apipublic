//
//  DeviceApiTest.swift
//  ApiTest
//
//  Created by Jesper Lundqvist on 2020-06-02.
//  Copyright © 2020 Virtual Stores. All rights reserved.
//

import XCTest
import Domain
import Api

class DeviceApiTest: XCTestCase {
    
    var api: Api!
    
    override func setUpWithError() throws {
        api = ApiImpl(centralConnection: TestEnvironment.centralConnection, clientId: TestEnvironment.clientId)
        api.initStoreApi(storeConnection: TestEnvironment.retailStoreConnection)
    }

    override func tearDownWithError() throws {
    }

    func testMinimumVersion() throws {
        let expectation = XCTestExpectation()
        api.store?.deviceApi.minimumVersion { result in
            switch result {
            case .success(let version):
                // Check if valid version
                XCTAssert(version.compare("0.0.0", options: .numeric) == .orderedDescending)
            case .failure(let error):
                XCTFail(error.localizedDescription)
            }
            expectation.fulfill()
        }

        wait(for: [expectation], timeout: 10.0)
    }
    
}
