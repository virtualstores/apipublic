//
//  ControlApiTest.swift
//  ApiTest
//
//  Created by Jesper Lundqvist on 2020-08-05.
//  Copyright © 2020 Virtual Stores. All rights reserved.
//

import XCTest
import Domain
import Api

class ControlApiTest: XCTestCase {
    
    var api: Api!
    let sessionId: Int64 = 1297
    let cartId: String = "5a08d936-57ef-439c-9dcf-cfcc0d0af185"
    let cartItemId: String = "20"
    let barcode = "7300206233008"
    
    
    override func setUpWithError() throws {
        api = ApiImpl(centralConnection: TestEnvironment.centralConnection)
        api.initStoreApi(storeConnection: TestEnvironment.developmentStoreConnection)
    }

    override func tearDownWithError() throws {
    }
    
    func testEmployeeVerify() throws {
        let expectation = XCTestExpectation()
        
        api.store?.controlApi.getCarts(forSession: sessionId, employeeId: TestEnvironment.constructionUser.memberNumber) { result in
            switch result {
            case .success(let response):
                XCTAssert(!response.posResponse.carts.isEmpty)
                
                if let cart = response.posResponse.carts.first {
                    print("\ncartId: \(cart.id)")
                    print("cartItemId: \(cart.items.first!.id)")
                    print("barcode: \(cart.items.first!.article!.barcodes!.first!)\n")
                }
            case .failure(let error):
                XCTFail(error.localizedDescription)
            }
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 10.0)
    }
    
    func testReportCart() throws {
        let expectation = XCTestExpectation()
        
        api.store?.controlApi.reportCart(forSession: sessionId, forceSuccess: true) { (error) in
            switch error {
            case .none:
                XCTAssert(error == nil)
            case .some(let error):
                XCTFail(error.localizedDescription)
            }
        }
        
        wait(for: [expectation], timeout: 10.0)
    }
    
    func testAddItem() throws {
        let expectation = XCTestExpectation()
        
        api.store?.controlApi.addItem(forSession: sessionId, cartId: cartId, barcode: barcode, quantity: 1.0) { result in
            switch result {
            case .success(let response):
                XCTAssert(!response.posResponse.carts.isEmpty)
            case .failure(let error):
                XCTFail(error.localizedDescription)
            }
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 10.0)
    }
    
    func testUpdateItem() throws {
        let expectation = XCTestExpectation()
        
        api.store?.controlApi.updateItem(forSession: sessionId, cartId: cartId, cartItemId: cartItemId, barcode: barcode, quantity: 3.0) { result in
            switch result {
            case .success(let response):
                XCTAssert(!response.posResponse.carts.isEmpty)
            case .failure(let error):
                XCTFail(error.localizedDescription)
            }
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 10.0)
    }
    
    func testDeleteItem() throws {
        let expectation = XCTestExpectation()
        
        api.store?.controlApi.deleteItem(forSession: sessionId, cartId: cartId, cartItemId: cartItemId, barcode: barcode) { result in
            switch result {
            case .success(let respose):
                XCTAssert(!respose.posResponse.carts.isEmpty)
            case .failure(let error):
                XCTFail(error.localizedDescription)
            }
        }
        
        wait(for: [expectation], timeout: 10.0)
    }
}
