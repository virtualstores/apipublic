//
//  EmployeeApiTest.swift
//  ApiTest
//
//  Created by Théodore Roos on 2020-08-03.
//  Copyright © 2020 Virtual Stores. All rights reserved.
//

import XCTest
import Domain
import Api

class EmployeeApiTest: XCTestCase {
    
    var api: Api!
    
    override func setUpWithError() throws {
        api = ApiImpl(centralConnection: TestEnvironment.centralConnection)
        api.initStoreApi(storeConnection: TestEnvironment.retailStoreConnection)
    }

    override func tearDownWithError() throws {
    }
    
    func testEmployeeVerify() throws {
        let expectation = XCTestExpectation()
        
        api.central.centralApi.verifyEmployee(clientId: 4, employeeIdentifier: "4903055:beijersuper@virtualstores.se", completion: { (result) in
            switch result {
            case .success(let employee):
                XCTAssert(employee.memberNumber == "4903055:beijersuper@virtualstores.se")
            case .failure(let error):
                XCTFail(error.localizedDescription)
            }
            expectation.fulfill()
        })
        
        wait(for: [expectation], timeout: 10.0)
    }

}
