//
//  ItemApiTest.swift
//  ApiTest
//
//  Created by Jesper Lundqvist on 2020-06-02.
//  Copyright © 2020 Virtual Stores. All rights reserved.
//

import XCTest
import Domain
import Api

class ItemApiTest: XCTestCase {
    
    var api: Api!
    
    override func setUpWithError() throws {
        api = ApiImpl(centralConnection: TestEnvironment.centralConnection, clientId: TestEnvironment.clientId)
        api.initStoreApi(storeConnection: TestEnvironment.retailStoreConnection)
    }

    override func tearDownWithError() throws {
    }
    
    func testItemById() throws {
        let expectation = XCTestExpectation()
        api.store?.itemApi.getItemBy(id: TestEnvironment.barcode) { result in
            switch result {
            case .success:
                break
            case .failure(let error):
                XCTFail(error.localizedDescription)
            }
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 10.0)
    }
    
    func testItemDetail() throws {
        let expectation = XCTestExpectation()
        api.store?.itemApi.getItemDetail(id: TestEnvironment.barcode) { result in
            switch result {
            case .success:
                break
            case .failure(let error):
                XCTFail(error.localizedDescription)
            }
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 10.0)
    }
    
    func testRecommendedItems() throws {
        let expectation = XCTestExpectation()
        api.store?.itemApi.getRecommendedItems(for: TestEnvironment.barcode) { result in
            switch result {
            case .success:
                break
            case .failure(let error):
                XCTFail(error.localizedDescription)
            }
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 10.0)
    }
    
    func testItemGroup() throws {
        api.initStoreApi(storeConnection: TestEnvironment.constructionStoreConnection)
        
        let expectation = XCTestExpectation()
        api.store?.itemApi.getItemGroup(limit: 1, offset: 0) { result in
            switch result {
            case .success:
                break
            case .failure(let error):
                XCTFail(error.localizedDescription)
            }
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 10.0)
    }
    
    func testItemGroupDetail() throws {
        api.initStoreApi(storeConnection: TestEnvironment.constructionStoreConnection)
        
        let expectation = XCTestExpectation()
        api.store?.itemApi.getItemGroupDetail(id: "5f865ab3-8ab7-4a0d-8554-d5550a97eab8") { result in
            switch result {
            case .success(let details):
                XCTAssert(details.subgroups.first?.name == "Byggnadsblock & Ballast")
                XCTAssert(details.items.first?.name == "FOGSAND 20KG")
            case .failure(let error):
                XCTFail(error.localizedDescription)
            }
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 10.0)
    }
    
}
