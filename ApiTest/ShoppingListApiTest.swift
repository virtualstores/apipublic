//
//  ShoppingListApiTest.swift
//  ApiTest
//
//  Created by Jesper Lundqvist on 2020-06-02.
//  Copyright © 2020 Virtual Stores. All rights reserved.
//

import XCTest
import Domain
import Api

class ShoppingListApiTest: XCTestCase {
    
    var api: Api!
    
    override func setUpWithError() throws {
        api = ApiImpl(centralConnection: TestEnvironment.centralConnection, clientId: TestEnvironment.clientId)
        api.initStoreApi(storeConnection: TestEnvironment.retailStoreConnection)
    }

    override func tearDownWithError() throws {
    }
    
    func testShoppingLists() throws {
        let expectation = XCTestExpectation()
        
        api.store?.shoppingListApi.getShoppingListsBy(customerId: TestEnvironment.retailUser.memberNumber) { result in
            switch result {
            case .success:
                break
            case .failure(let error):
                XCTFail(error.localizedDescription)
            }
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 10.0)
    }
    
}
