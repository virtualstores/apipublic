//
//  ShelfApiTest.swift
//  ApiTest
//
//  Created by Jesper Lundqvist on 2020-06-02.
//  Copyright © 2020 Virtual Stores. All rights reserved.
//

import XCTest
import Domain
import Api

class ShelfApiTest: XCTestCase {
    
    var api: Api!
    
    override func setUpWithError() throws {
        api = ApiImpl(centralConnection: TestEnvironment.centralConnection, clientId: TestEnvironment.clientId)
        api.initStoreApi(storeConnection: TestEnvironment.retailStoreConnection)
    }

    override func tearDownWithError() throws {
    }
    
    func testShelfGroups() throws {
        let expectation = XCTestExpectation()
        
        api.store?.shelfApi.getShelfGroups { result in
            switch result {
            case .success(let groups):
                XCTAssert(groups.first?.itemPosition.x == 632)
            case .failure(let error):
                XCTFail(error.localizedDescription)
            }
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 10.0)
    }
    
    func testPositionItem() throws {
        let expectation = XCTestExpectation()
        
        api.store?.shelfApi.getShelfGroups { result in
            switch result {
            case .success(let groups):
                self.api.store?.shelfApi.positionOnShelf(id: groups.first!.id, tierPosition: 0, barcode: "98989898", overwrite: false) { error in
                    if let error = error {
                        XCTFail(error.localizedDescription)
                    }
                    else {
                        self.api.store?.shelfApi.removeFromShelf(barcode: "98989898", completion: { error in
                            if let error = error {
                                XCTFail(error.localizedDescription)
                            }
                        })
                    }
                }
            case .failure(let error):
                XCTFail(error.localizedDescription)
            }
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 10.0)
    }
}
