//
//  TestEnvironment.swift
//  ApiTest
//
//  Created by Jesper Lundqvist on 2020-06-02.
//  Copyright © 2020 Virtual Stores. All rights reserved.
//

import Domain

enum TestEnvironment {
    static let centralConnection = ServerConnection(id: 0, storeId: 0, serverUrl: "https://central-dev.aws.vs-office.se", mqttUrl: "", apiKey: "n0xnN8Vrzk7hXT4HhYj0")
    static let retailStoreConnection = ServerConnection(id: 0, storeId: 1, serverUrl: "https://hemkop-dev-local.aws.vs-office.se", mqttUrl: "", apiKey: "kanelbulle")
    static let constructionStoreConnection = ServerConnection(id: 0, storeId: 1, serverUrl: "https://beijer-dev-local.aws.vs-office.se", mqttUrl: "", apiKey: "kanelbulle")
    static let developmentStoreConnection = ServerConnection(id: 0, storeId: 1, serverUrl: "https://gunnis-hp-local.ih.vs-office.se", mqttUrl: "", apiKey: "kanelbulle")
    static let gunnisThinkPad = ServerConnection(id: 1, storeId: 13, serverUrl: "http://192.168.1.39:5000", mqttUrl: "", apiKey: "kanelbulle")

    static let retailUser = User(name: "Jesper", socialSecurity: "9710213159", memberNumber: "9752299168235157")
    static let constructionUser = User(name: "Martin", socialSecurity: nil, memberNumber: "4903055:martin.prevorsek@virtualstores.se")
    
    static let barcode = "7310760012896"
}
